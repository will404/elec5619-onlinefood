<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon"
	href="${pageContext.request.contextPath }/admin/img/favicon.png">
<title>Index</title>
<link
	href="${pageContext.request.contextPath }/admin/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath }/admin/css/bootstrap-theme.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath }/admin/css/elegant-icons-style.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath }/admin/css/font-awesome.min.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath }/admin/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath }/admin/assets/fullcalendar/fullcalendar/fullcalendar.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath }/admin/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css"
	rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath }/admin/css/owl.carousel.css"
	type="text/css">
<link
	href="${pageContext.request.contextPath }/admin/css/jquery-jvectormap-1.2.2.css"
	rel="stylesheet">
<link rel="stylesheet"
	href="${pageContext.request.contextPath }/admin/css/fullcalendar.css">
<link href="${pageContext.request.contextPath }/admin/css/widgets.css"
	rel="stylesheet">
<link href="${pageContext.request.contextPath }/admin/css/style.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath }/admin/css/style-responsive.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath }/admin/css/xcharts.min.css"
	rel=" stylesheet">
<link
	href="${pageContext.request.contextPath }/admin/css/jquery-ui-1.10.4.min.css"
	rel="stylesheet">
</head>

<body ng-app="app" ng-controller="msCtrl">
	<section id="container" class="">
		<!--main content start-->
		<section id="main-content">
			<section class="wrapper">
				<!-- Form validations -->
				<div class="row">
					<div class="col-lg-12">
						<section class="panel">
							<header class="panel-heading">Update MealSeries</header>
							<div class="panel-body">
								<div class="form">
									<form class="form-validate form-horizontal" id="feedback_form"
										method="get" action="">
										<div class="form-group ">
											<label for="cname" class="control-label col-lg-2">MealSeries
												Name <span class="required">*</span>
											</label>
											<div class="col-lg-10">
												<input class="form-control"  ng-model="msName" name="fullname"
													minlength="1" type="text" required />
											</div>
										</div>
										<div class="form-group">
											<div class="text-center col-lg-10">
												<span class="btn btn-primary" ng-click="updateMs()">Update</span>
											</div>
										</div>
									</form>
								</div>
							</div>
						</section>
					</div>
				</div>
			</section>
		</section>
		<!--main content end-->
	</section>
	<!-- container section start -->

	<!-- javascripts -->
	<script src="${pageContext.request.contextPath }/admin/js/jquery.js"></script>
	<script
		src="${pageContext.request.contextPath }/admin/js/bootstrap.min.js"></script>
	<!-- nice scroll -->
	<script
		src="${pageContext.request.contextPath }/admin/js/jquery.scrollTo.min.js"></script>
	<script
		src="${pageContext.request.contextPath }/admin/js/jquery.nicescroll.js"
		type="text/javascript"></script>
	<!-- jquery validate js -->
	<script type="text/javascript"
		src="${pageContext.request.contextPath }/admin/js/jquery.validate.min.js"></script>

	<!-- custom form validation script for this page-->
	<script
		src="${pageContext.request.contextPath }/admin/js/form-validation-script.js"></script>
	<!--custome script for all page-->
	<script src="${pageContext.request.contextPath }/admin/js/scripts.js"></script>
	<script src="${pageContext.request.contextPath }/common/js/angular.1.5.5.min.js"></script>

</body>

<script type="text/javascript">
	var app = angular.module("app",[]);
	app.controller("msCtrl",function($scope,$http){
		var id =document.URL.split("?")[1].split("=")[1];
		var url = "${pageContext.request.contextPath}/mealSeries/getMS.action?id="+id;
	  $http.post(url)
	    .then(function (result) {
	        $scope.ms = result.data;
	        $scope.msName = $scope.ms.seriesName;
	    });
	  $scope.updateMs = function() {
		  if($scope.msName== null || $scope.msName == "") {
				alert("MealSeries Name cann't be null");
				return;
			}
			var url = "${pageContext.request.contextPath}/mealSeries/updateMS.action";
			 $.ajax({
		    		type : "post", 
					url : url,
					data :{
						"seriesName":$scope.msName,
						"id":$scope.ms.id
					},
					success : function(data) {
						if(data == 2) {
							alert("the meal series has already exist");
							return;
						}else {
							alert("update meal series success");
							window.close();
						}			
					}
		    	});
	  }
	});
</script>
</html>
