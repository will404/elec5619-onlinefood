<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon"
	href="${pageContext.request.contextPath }/admin/img/favicon.png">
<title>Index</title>
<link
	href="${pageContext.request.contextPath }/admin/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath }/admin/css/bootstrap-theme.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath }/admin/css/elegant-icons-style.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath }/admin/css/font-awesome.min.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath }/admin/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath }/admin/assets/fullcalendar/fullcalendar/fullcalendar.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath }/admin/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css"
	rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath }/admin/css/owl.carousel.css"
	type="text/css">
<link
	href="${pageContext.request.contextPath }/admin/css/jquery-jvectormap-1.2.2.css"
	rel="stylesheet">
<link rel="stylesheet"
	href="${pageContext.request.contextPath }/admin/css/fullcalendar.css">
<link href="${pageContext.request.contextPath }/admin/css/widgets.css"
	rel="stylesheet">
<link href="${pageContext.request.contextPath }/admin/css/style.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath }/admin/css/style-responsive.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath }/admin/css/xcharts.min.css"
	rel=" stylesheet">
<link
	href="${pageContext.request.contextPath }/admin/css/jquery-ui-1.10.4.min.css"
	rel="stylesheet">
</head>

<body ng-app="app" ng-controller="mslistCtrl">
	<section id="container" class="">
		<header class="header dark-bg">
			<div class="toggle-nav">
				<div class="icon-reorder tooltips"
					data-original-title="Toggle Navigation" data-placement="bottom"></div>
			</div>
			<a href="#" class="logo">Online <span class="lite">Food</span></a>

			<div class="top-nav notification-row">
				<!-- notificatoin dropdown start-->
				<ul class="nav pull-right top-menu">
					<!-- user login dropdown start-->
					<li class="dropdown"><a data-toggle="dropdown"
						class="dropdown-toggle" href="#"> <span class="profile-ava">
								<img width="40px" height="40px" alt="" src="${pageContext.request.contextPath }/admin/img/userdefault.jpg">
						</span> <span class="username">${Admin }</span> <b class="caret"></b>
					</a>
						<ul class="dropdown-menu extended logout">
							<div class="log-arrow-up"></div>
							<li><a href="${pageContext.request.contextPath }/user/adminExit.action"><i class="icon_key_alt"></i>
									Log Out</a></li>
						</ul></li>
					<!-- user login dropdown end -->
				</ul>
				<!-- notificatoin dropdown end-->
			</div>
		</header>
		<!--header end-->
		<!--sidebar start-->
		<aside>
			<div id="sidebar" class="nav-collapse ">
				<!-- sidebar menu start-->
				<ul class="sidebar-menu">
					<li ><a class="" href="${pageContext.request.contextPath }/admin/index.jsp"> <i
							class="icon_house_alt"></i> <span>Home</span>
					</a></li>
					<li class="sub-menu active"><a href="javascript:;" class=""> <i
							class="icon_genius"></i> <span>MealSeries</span> <span
							class="menu-arrow arrow_carrot-right"></span>
					</a>
						<ul class="sub">
							<li><a class="" href="${pageContext.request.contextPath }/admin/addMealSeries.jsp">Add MealSeries</a></li>
							<li><a  href="${pageContext.request.contextPath }/admin/listMealSeries.jsp"><span>MealSeries
										List</span></a></li>
						</ul></li>

					<li class="sub-menu"><a href="javascript:;" class=""> <i
							class="icon_documents_alt"></i> <span>Meal Manage</span> <span
							class="menu-arrow arrow_carrot-right"></span>
					</a>
						<ul class="sub">
							<li><a class="" href="${pageContext.request.contextPath }/admin/listMeal.jsp"><span>Meal List</span></a></li>
						</ul></li>
					<li class="sub-menu"><a href="javascript:;" class=""> <i
							class="icon_table"></i> <span>Order Manage</span> <span
							class="menu-arrow arrow_carrot-right"></span>
					</a>
						<ul class="sub">
							<li><a class="" href="${pageContext.request.contextPath }/admin/listOrders.jsp">Order List</a></li>
						</ul></li>



				</ul>
				<!-- sidebar menu end-->
			</div>
		</aside>
		<!--sidebar end-->

		<!--main content start-->
		<section id="main-content">
			<section class="wrapper">
				<div class="row">
					<div class="col-lg-12">
						<h3 class="page-header">
							<i class="fa fa-files-o"></i>MealSeries List
						</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a href="${pageContext.request.contextPath }/admin/index.jsp">Home</a></li>
							<li><i class="icon_document_alt"></i>MealSeries List</li>
						</ol>
					</div>
				</div>
				<!-- Form validations -->
				<div class="row">
                  <div class="col-md-offset-2 col-lg-8">
                      <section class="panel">
                          <header class="panel-heading">
                              MealSeries List
                          </header>
                          <table class="table table-striped table-advance table-hover">
                           <tbody>
                              <tr>
                                 <th><i class="icon_id"></i>id</th>
                                 <th><i class="icon_info"></i>SeriesName</th>
                                 <th><i class="icon_cogs"></i> Action</th>
                              </tr>
                             
                              <tr ng-repeat="ms in mslist">
                                 <td width="40%"> {{ms.id}}</td>
                                 <td width="40%">{{ms.seriesName}}</td>
                                 <td width="20%">
                                  <div class="btn-group">
                                      <a class="btn btn-primary popovers" href="#" ng-click="updateMS(ms.id)"><i class="icon_document_alt"></i></a>
                                      <a class="btn btn-danger popovers" href="#" ng-click="deleteMs(ms.id)"><i class="icon_close_alt2"></i></a>
                                  </div>
                                  </td>
                              </tr>
                                                      
                           </tbody>
                           <tfoot>
								<tr>
								<td colspan="8" class="text-center">cur:<span>{{curPage}}</span>/<span>{{allPage}}</span> &nbsp;&nbsp;
									<a href="#" ng-click="init_page_data('H')">Head</a>&nbsp;
									 <a href="#" ng-click="init_page_data('P')">Pre</a>&nbsp;
									 <a href="#" ng-click="init_page_data('N')">Next</a>&nbsp;
									 <a href="#" ng-click="init_page_data('T')"> Tail</a>
								</td>
								</tr>
							</tfoot>
                        </table>
                      </section>
                  </div>
              </div>
              
			</section>
		</section>
	
		<!--main content end-->
	</section>
	<!-- container section start -->

	<!-- javascripts -->
	<script src="${pageContext.request.contextPath }/admin/js/jquery.js"></script>
	<script
		src="${pageContext.request.contextPath }/admin/js/bootstrap.min.js"></script>
	<!-- nice scroll -->
	<script
		src="${pageContext.request.contextPath }/admin/js/jquery.scrollTo.min.js"></script>
	<script
		src="${pageContext.request.contextPath }/admin/js/jquery.nicescroll.js"
		type="text/javascript"></script>
	<!-- jquery validate js -->
	<script type="text/javascript"
		src="${pageContext.request.contextPath }/admin/js/jquery.validate.min.js"></script>

	<!-- custom form validation script for this page-->
	<script
		src="${pageContext.request.contextPath }/admin/js/form-validation-script.js"></script>
	<!--custome script for all page-->
	<script src="${pageContext.request.contextPath }/admin/js/scripts.js"></script>
	<script src="${pageContext.request.contextPath }/common/js/angular.1.5.5.min.js"></script>
	<script src="${pageContext.request.contextPath }/common/layer/layer.js"></script>

</body>

<script type="text/javascript">
	var app = angular.module("app",[]);
	app.controller("mslistCtrl",function($scope,$http){
		$scope.curPage = 1;
		$scope.init_page = function() {
			var url = "${pageContext.request.contextPath}/mealSeries/countMSPage.action";
			$http.get(url).then(function(result){
				$scope.allPage = result.data;
			});
		}
		$scope.init_page_data = function(option)	{
			if(option == "H"){
				$scope.curPage = 1;
			}
			if(option == "T"){
				$scope.curPage = $scope.allPage;
			}
			if(option == "N") {
		
				if($scope.curPage <  $scope.allPage){
					++$scope.curPage;
				}
			}
			if(option == "P") {
				if($scope.curPage - 1 >= 1){
					--$scope.curPage;
				}
			}
			var url = "${pageContext.request.contextPath}/mealSeries/findMSPage.action?page="+$scope.curPage;
			  $http.post(url)
			    .then(function (result) {
			        $scope.mslist = result.data;
			    });
		};		
		
		$scope.deleteMs = function(id) {
			layer.confirm("   do you want to delete this?", {
			  btn: ["yes","no"]  
			,title:"Warning"}, function(){
				var url = "${pageContext.request.contextPath}/mealSeries/deleteMS.action?id="+id;
				  $http.post(url)
				    .then(function (result) {
				    	if(result.data == "0") {
				    		layer.msg("cann't delete, the Meal series isn't empty", {
							    time: 2000,
							    btn: ["see it"]
							  });
				    	}else {
				    		layer.msg("delete success", {icon: 1});
					    	$scope.init_page();
							$scope.init_page_data(0);
				    	}
				});
			  
			}, function(){
			});
			
		}
		$scope.updateMS = function(id) {
			layer.open({
			  type: 2,
			  area: ["500px", "450px"],
			  fixed: false, 
			  maxmin: false,
			  title:"update Meal Series",
			  cancel: function(index, layero){ 
				    layer.close(index);
				    
				    $scope.init_page_data(0);		  
				},
			  content: "${pageContext.request.contextPath }/admin/updateMealSeries.jsp?id="+id
			});
		}
		$scope.init_page();
		$scope.init_page_data(0);
	});
</script>
</html>
