<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon"
	href="${pageContext.request.contextPath }/admin/img/favicon.png">
<title>Index</title>
<link
	href="${pageContext.request.contextPath }/admin/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath }/admin/css/bootstrap-theme.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath }/admin/css/elegant-icons-style.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath }/admin/css/font-awesome.min.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath }/admin/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath }/admin/assets/fullcalendar/fullcalendar/fullcalendar.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath }/admin/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css"
	rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath }/admin/css/owl.carousel.css"
	type="text/css">
<link
	href="${pageContext.request.contextPath }/admin/css/jquery-jvectormap-1.2.2.css"
	rel="stylesheet">
<link rel="stylesheet"
	href="${pageContext.request.contextPath }/admin/css/fullcalendar.css">
<link href="${pageContext.request.contextPath }/admin/css/widgets.css"
	rel="stylesheet">
<link href="${pageContext.request.contextPath }/admin/css/style.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath }/admin/css/style-responsive.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath }/admin/css/xcharts.min.css"
	rel=" stylesheet">
<link
	href="${pageContext.request.contextPath }/admin/css/jquery-ui-1.10.4.min.css"
	rel="stylesheet">
</head>

<body ng-app="app" ng-controller="msCtrl">
	<section id="container" class="">
		<!--main content start-->
		<section id="main-content">
			<section class="wrapper">
				<!-- Form validations -->
				<div class="row">
					<div class="col-lg-12">
						<section class="panel">
							<header class="panel-heading">Order infomation</header>
							<section class="panel">
								<div class="panel-body bio-graph-info">
									<div class="row">
										<div class="bio-row">
											<p>
												<span>User name </span>: {{o.user.displayName}}
											</p>
										</div>
										<div class="bio-row">
											<p>
												<span>orderTime </span>: {{o.orderTimeStr}}
											</p>
										</div>
										<div class="bio-row">
											<p>
												<span>orderPrice</span>: {{o.orderPriceDouble}}
											</p>
										</div>
										<div class="bio-row">
											<p>
												<span>orderState </span>: {{o.orderState}}
											</p>
										</div>
										<div class="bio-row">
											<p>
												<span>addOn </span>: {{o.addOn}}
											</p>
										</div>
									</div>
									<div class="table-responsive">
										<h4 class="text-center">order items</h4>
										<table class="table">
											<tbody>
												<tr>
													<th>#</th>
													<th>MealName</th>
													<th>mealPrice</th>
													<th>mealCount</th>
												</tr>

												<tr ng-repeat="oi in o.ordersItemSet">
													<td><img width="50px" height="50px" alt="" src="{{oi.meal.mealImage}}"></td>
													<td>{{oi.meal.mealName}}</td>
													<td>{{oi.mealPriceDouble}}</td>
													<td>{{oi.mealCount}}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</section>
					</div>
				</div>
			</section>
		</section>
		<!--main content end-->
	</section>
	<!-- container section start -->

	<!-- javascripts -->
	<script src="${pageContext.request.contextPath }/admin/js/jquery.js"></script>
	<script
		src="${pageContext.request.contextPath }/admin/js/bootstrap.min.js"></script>
	<!-- nice scroll -->
	<script
		src="${pageContext.request.contextPath }/admin/js/jquery.scrollTo.min.js"></script>
	<script
		src="${pageContext.request.contextPath }/admin/js/jquery.nicescroll.js"
		type="text/javascript"></script>
	<!-- jquery validate js -->
	<script type="text/javascript"
		src="${pageContext.request.contextPath }/admin/js/jquery.validate.min.js"></script>

	<!-- custom form validation script for this page-->
	<script
		src="${pageContext.request.contextPath }/admin/js/form-validation-script.js"></script>
	<!--custome script for all page-->
	<script src="${pageContext.request.contextPath }/admin/js/scripts.js"></script>
	<script
		src="${pageContext.request.contextPath }/common/js/angular.1.5.5.min.js"></script>

</body>

<script type="text/javascript">
	var app = angular.module("app",[]);
	app.controller("msCtrl",function($scope,$http){
		var id =document.URL.split("?")[1].split("=")[1];
		var url = "${pageContext.request.contextPath}/orders/getOrders.action?id="+id;
	  $http.post(url)
	    .then(function (result) {
	        $scope.o = result.data;
	    });
	});
</script>
</html>
