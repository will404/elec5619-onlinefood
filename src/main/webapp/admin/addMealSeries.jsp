<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon"
	href="${pageContext.request.contextPath }/admin/img/favicon.png">
<title>Index</title>
<link
	href="${pageContext.request.contextPath }/admin/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath }/admin/css/bootstrap-theme.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath }/admin/css/elegant-icons-style.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath }/admin/css/font-awesome.min.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath }/admin/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath }/admin/assets/fullcalendar/fullcalendar/fullcalendar.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath }/admin/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css"
	rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath }/admin/css/owl.carousel.css"
	type="text/css">
<link
	href="${pageContext.request.contextPath }/admin/css/jquery-jvectormap-1.2.2.css"
	rel="stylesheet">
<link rel="stylesheet"
	href="${pageContext.request.contextPath }/admin/css/fullcalendar.css">
<link href="${pageContext.request.contextPath }/admin/css/widgets.css"
	rel="stylesheet">
<link href="${pageContext.request.contextPath }/admin/css/style.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath }/admin/css/style-responsive.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath }/admin/css/xcharts.min.css"
	rel=" stylesheet">
<link
	href="${pageContext.request.contextPath }/admin/css/jquery-ui-1.10.4.min.css"
	rel="stylesheet">
</head>

<body ng-app="app" ng-controller="msCtrl">
	<section id="container" class="">
		<header class="header dark-bg">
			<div class="toggle-nav">
				<div class="icon-reorder tooltips"
					data-original-title="Toggle Navigation" data-placement="bottom"></div>
			</div>
			<a href="#" class="logo">Online <span class="lite">Food</span></a>

			<div class="top-nav notification-row">
				<!-- notificatoin dropdown start-->
				<ul class="nav pull-right top-menu">
					<!-- user login dropdown start-->
					<li class="dropdown"><a data-toggle="dropdown"
						class="dropdown-toggle" href="#"> <span class="profile-ava">
								<img width="40px" height="40px" src="${pageContext.request.contextPath }/admin/img/userdefault.jpg">
						</span> <span class="username">${Admin }</span> <b class="caret"></b>
					</a>
						<ul class="dropdown-menu extended logout">
							<div class="log-arrow-up"></div>
							<li><a href="${pageContext.request.contextPath }/user/adminExit.action"><i class="icon_key_alt"></i>
									Log Out</a></li>
						</ul></li>
					<!-- user login dropdown end -->
				</ul>
				<!-- notificatoin dropdown end-->
			</div>
		</header>
		<!--header end-->
		<!--sidebar start-->
		<aside>
			<div id="sidebar" class="nav-collapse ">
				<!-- sidebar menu start-->
				<ul class="sidebar-menu">
					<li><a class="" href="${pageContext.request.contextPath }/admin/index.jsp"> <i
							class="icon_house_alt"></i> <span>Home</span>
					</a></li>
					<li class="sub-menu active"><a href="javascript:;" class=""> <i
							class="icon_genius"></i> <span>MealSeries</span> <span
							class="menu-arrow arrow_carrot-right"></span>
					</a>
						<ul class="sub">
							<li><a  href="${pageContext.request.contextPath }/admin/addMealSeries.jsp">Add MealSeries</a></li>
							<li><a class="" href="${pageContext.request.contextPath }/admin/listMealSeries.jsp"><span>MealSeries
										List</span></a></li>
						</ul></li>

					<li class="sub-menu"><a href="javascript:;" class=""> <i
							class="icon_documents_alt"></i> <span>Meal Manage</span> <span
							class="menu-arrow arrow_carrot-right"></span>
					</a>
						<ul class="sub">
							<li><a class="" href="${pageContext.request.contextPath }/admin/listMeal.jsp"><span>Meal List</span></a></li>
						</ul></li>
					<li class="sub-menu"><a href="javascript:;" class=""> <i
							class="icon_table"></i> <span>Order Manage</span> <span
							class="menu-arrow arrow_carrot-right"></span>
					</a>
						<ul class="sub">
							<li><a class="" href="${pageContext.request.contextPath }/admin/listOrders.jsp">Order List</a></li>
						</ul></li>



				</ul>
				<!-- sidebar menu end-->
			</div>
		</aside>
		<!--sidebar end-->

		<!--main content start-->
		<section id="main-content">
			<section class="wrapper">
				<div class="row">
					<div class="col-lg-12">
						<h3 class="page-header">
							<i class="fa fa-files-o"></i> Add MealSeries
						</h3>
						<ol class="breadcrumb">
							<li><i class="fa fa-home"></i><a href="${pageContext.request.contextPath }/admin/index.jsp">Home</a></li>
							<li><i class="icon_document_alt"></i>Add MealSeries</li>
						</ol>
					</div>
				</div>
				<!-- Form validations -->
				<div class="row">
					<div class="col-lg-12">
						<section class="panel">
							<header class="panel-heading">Add MealSeries</header>
							<div class="panel-body">
								<div class="form">
									<form class="form-validate form-horizontal" id="feedback_form"
										method="get" action="">
										<div class="form-group ">
											<label for="cname" class="control-label col-lg-2">MealSeries
												Name <span class="required">*</span>
											</label>
											<div class="col-lg-10">
												<input class="form-control"  ng-model="msName" name="fullname"
													minlength="1" type="text" required />
											</div>
										</div>
										<div class="form-group">
											<div class="col-lg-offset-5 col-lg-10">
												<span class="btn btn-primary" ng-click="addMs()">Add</span>
												<span class="btn btn-default">Cancel</span>
											</div>
										</div>
									</form>
								</div>
							</div>
						</section>
					</div>
				</div>
			</section>
		</section>
		<!--main content end-->
	</section>
	<!-- container section start -->

	<!-- javascripts -->
	<script src="${pageContext.request.contextPath }/admin/js/jquery.js"></script>
	<script
		src="${pageContext.request.contextPath }/admin/js/bootstrap.min.js"></script>
	<!-- nice scroll -->
	<script
		src="${pageContext.request.contextPath }/admin/js/jquery.scrollTo.min.js"></script>
	<script
		src="${pageContext.request.contextPath }/admin/js/jquery.nicescroll.js"
		type="text/javascript"></script>
	<!-- jquery validate js -->
	<script type="text/javascript"
		src="${pageContext.request.contextPath }/admin/js/jquery.validate.min.js"></script>

	<!-- custom form validation script for this page-->
	<script
		src="${pageContext.request.contextPath }/admin/js/form-validation-script.js"></script>
	<!--custome script for all page-->
	<script src="${pageContext.request.contextPath }/admin/js/scripts.js"></script>
	<script src="${pageContext.request.contextPath }/common/js/angular.1.5.5.min.js"></script>

</body>

<script type="text/javascript">
	var app = angular.module("app",[]);
	app.controller("msCtrl",function($scope,$http){
		$scope.addMs = function() {
			if($scope.msName== null || $scope.msName == "") {
				alert("MealSeries cann't be null");
				return;
			}
			
			var url = "${pageContext.request.contextPath}/mealSeries/addMS.action";
			 $.ajax({
		    		type : "post", 
					url : url,
					data :{
						"seriesName":$scope.msName
					},
					
					success : function(data) {
						if(data == 2) {
							alert("the meal series has already exist");
							return;
						}else {
							alert("add meal series success");
						}			
					}
		    	});
		}
		
	});
</script>
</html>
