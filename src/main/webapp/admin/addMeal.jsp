<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon"
	href="${pageContext.request.contextPath }/admin/img/favicon.png">
<title>Index</title>
<link
	href="${pageContext.request.contextPath }/admin/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath }/admin/css/bootstrap-theme.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath }/admin/css/elegant-icons-style.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath }/admin/css/font-awesome.min.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath }/admin/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath }/admin/assets/fullcalendar/fullcalendar/fullcalendar.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath }/admin/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css"
	rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath }/admin/css/owl.carousel.css"
	type="text/css">
<link
	href="${pageContext.request.contextPath }/admin/css/jquery-jvectormap-1.2.2.css"
	rel="stylesheet">
<link rel="stylesheet"
	href="${pageContext.request.contextPath }/admin/css/fullcalendar.css">
<link href="${pageContext.request.contextPath }/admin/css/widgets.css"
	rel="stylesheet">
<link href="${pageContext.request.contextPath }/admin/css/style.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath }/admin/css/style-responsive.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath }/admin/css/xcharts.min.css"
	rel=" stylesheet">
<link
	href="${pageContext.request.contextPath }/admin/css/jquery-ui-1.10.4.min.css"
	rel="stylesheet">
</head>

<body ng-app="app" ng-controller="msCtrl">
	<section id="container" class="">
		<!--main content start-->
		<section id="main-content">
			<section class="wrapper">
				<div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                             Add Meal
                          </header>
                          <div class="panel-body">
                              <form class="form-horizontal " id="formId" method="get">
                                
                                  <div class="form-group">
                                      <label class="col-sm-2 control-label">MealSeries</label>
                                      <div class="col-sm-10">
                                           <select class="form-control m-bot15" id="msId">
                                          </select>
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-sm-2 control-label">mealName</label>
                                      <div class="col-sm-10">
                                          <input class="form-control" ng-model="mealName" type="text" placeholder="mealName">
                                      </div>
                                  </div>
                                    <div class="form-group">
                                      <label class="col-sm-2 control-label">mealPrice</label>
                                      <div class="col-sm-10">
                                          <input class="form-control price" ng-model="mealPrice" type="text" placeholder="12.20">
                                      </div>
                                  </div>
                                  
                                  <div class="form-group">
                                      <label class="col-sm-2 control-label">mealSummarize</label>
                                      <div class="col-sm-10">
                                          <input class="form-control" ng-model="mealSummarize" type="text" placeholder="mealSummarize">
                                      </div>
                                  </div>
                                  <div class="form-group ">
                                      <label for="ccomment" class="control-label col-lg-2">mealDescription</label>
                                      <div class="col-lg-10">
                                          <textarea class="form-control " ng-model="mealDescription" name="comment"></textarea>
                                      </div>
                                  </div>
                                  
                                   <div class="form-group">
                                      <label for="exampleInputFile"  class="control-label col-lg-2">mealImage</label>
                                      <div class="col-lg-10">
	                                      <input type="file" id="imgId">
	                                      <p class="help-block">please upload meal image.</p>
                                      </div>
                                  </div>
                                   <span class="btn btn-primary" ng-click="add()">Submit</span>
                              </form>
                          </div>
                      </section>
                  </div>
			</section>
		</section>
		<!--main content end-->
	</section>
	<!-- container section start -->

	<!-- javascripts -->
	<script src="${pageContext.request.contextPath }/admin/js/jquery.js"></script>
	<script
		src="${pageContext.request.contextPath }/admin/js/bootstrap.min.js"></script>
	<!-- nice scroll -->
	<script
		src="${pageContext.request.contextPath }/admin/js/jquery.scrollTo.min.js"></script>
	<script
		src="${pageContext.request.contextPath }/admin/js/jquery.nicescroll.js"
		type="text/javascript"></script>
	<!-- jquery validate js -->
	<script type="text/javascript"
		src="${pageContext.request.contextPath }/admin/js/jquery.validate.min.js"></script>

	<!-- custom form validation script for this page-->
	<script
		src="${pageContext.request.contextPath }/admin/js/form-validation-script.js"></script>
	<!--custome script for all page-->
	<script src="${pageContext.request.contextPath }/admin/js/scripts.js"></script>
	<script src="${pageContext.request.contextPath }/common/js/angular.1.5.5.min.js"></script>

</body>
<script type="text/javascript">
function isNumber(keyCode,flag) {
	
	// 数字
	if (keyCode >= 48 && keyCode <= 57 ) 
		return true;
	// 小数字键盘
	if (keyCode >= 96 && keyCode <= 105) 
		return true;
	if(flag){
		if(keyCode == 110 || keyCode == 190) 
			return true;
	}

	// Backspace键
	if (keyCode == 8) 
		return true;
	return false;
} 

$(function(){
	$(".onlynumber").keydown(function(event){
		return isNumber(event.keyCode,false);
	});
	$(".price").keydown(function(event){
		return isNumber(event.keyCode,true);
	});
	$(".price").blur(function(){
		var rex = /^[0-9]{0,}[.]{0,1}[0-9]{0,2}$/;
		if($(this).val().trim()!=""){
			if(!rex.test($(this).val())){
				alert("price input error!");
				$(this).val("");
			}
		}
	});
});

</script>
<script type="text/javascript">
	var app = angular.module("app",[]);
	app.controller("msCtrl",function($scope,$http){
		var url = "${pageContext.request.contextPath}/mealSeries/findMSAll.action";
		$.ajax({
			type : "post", 
			url : url, 
			data :null,
			success : function(data) {
				for(var i = 0; i<data.length;++i){
					$("#msId").append("<option value='"+data[i].id+"'>"+data[i].seriesName+"</option>");
				}
					
			}
		});
		
		$scope.add = function() {
			var fd = new FormData();
	       

	        if($scope.mealName == "" || $scope.mealName == null){
				alert("please input meal name");
				return;
			}
	        if($scope.mealPrice == "" || $scope.mealPrice == null){
				alert("please input meal price");
				return;
			}
	        if($scope.mealSummarize == "" || $scope.mealSummarize == null){
				alert("please input meal Summarize");
				return;
			}
	        if($scope.mealDescription == "" || $scope.mealDescription == null){
				alert("please input meal Description");
				return;
			}
	        var file = document.querySelector('input[type=file]').files[0];
	        if(file == null || file == "") {
	        	alert("please upload meal image");
				return;
	        }
	        fd.append("mealName", $scope.mealName); 
	        fd.append("mealPrice", $scope.mealPrice*100); 
	        fd.append("mealSummarize", $scope.mealSummarize); 
	        fd.append("mealDescription", $scope.mealDescription); 
	        fd.append("seriesId",$("#msId").val()); 
	       	fd.append("file", file); 
	        $http({
	              method:'POST',
	              url:"${pageContext.request.contextPath}/meal/addMeal.action",
	              data: fd,
	              headers: {'Content-Type':undefined},
	              transformRequest: angular.identity 
	               })   
	              .success( function ( msg ){
            		  alert("Add Meal Success");
            		  $("#formId")[0].reset()
	              });
	       	
		}
	});
</script>
<script type="text/javascript">

</script>
</html>
