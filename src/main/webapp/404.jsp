<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="${pageContext.request.contextPath }/admin/img/favicon.png">
    <title>404</title>
    <link href="${pageContext.request.contextPath }/admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath }/admin/css/bootstrap-theme.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath }/admin/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath }/admin/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath }/admin/css/style.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath }/admin/css/style-responsive.css" rel="stylesheet" />
</head>
  <body>
   <div class="page-404">
    <p class="text-404">404</p>

    <h2>Page Missing!</h2>
    <p>Something went wrong or that page does not exist yet. <br><a href="${pageContext.request.contextPath }/admin/index.jsp">Return Home</a></p>
  </div>
  
  </body>
</html>
