<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="${pageContext.request.contextPath }/img/favicon.png">

    <title>Login</title>

    <link href="${pageContext.request.contextPath }/admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath }/admin/css/bootstrap-theme.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath }/admin/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath }/admin/css/font-awesome.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath }/admin/css/style.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath }/admin/css/style-responsive.css" rel="stylesheet" />
</head>
  <body class="login-img3-body">
    <div class="container">

      <form class="login-form" action="index.html">        
        <div class="login-wrap">
            <p class="login-img"><i class="icon_lock_alt"></i></p>
            <div class="input-group">
              <span class="input-group-addon"><i class="icon_profile"></i></span>
              <input type="text" class="form-control" id="name" placeholder="Username" autofocus>
            </div>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon_key_alt"></i></span>
                <input type="password" class="form-control" id="pwd" placeholder="Password">
            </div>
            <span class="btn btn-primary btn-lg btn-block" id="adminLog" >Login</span>
        </div>
      </form>
    </div>
  </body>
  <script src="${pageContext.request.contextPath }/admin/js/jquery.js"></script>
  <script type="text/javascript">
  	$(function() {
  		$("#adminLog").click(function() {
  			if($("#name").val().trim()==""){
  				alert("please input username");
  				return;
  			}
  			if($("#pwd").val().trim()==""){
  				alert("please input password");
  				return;
  			}
  			$.ajax({
  				type : "post", 
  				url : "${pageContext.request.contextPath }/user/adminLogin.action", 
  				data :{
  					"username":$("#name").val(),
  					"password":$("#pwd").val()
  				},
  				success : function(data) {
  					if(data == "2") {
  						alert("username wrong!");
  						return;
  						
  					}
  					if(data=="3"){
  						alert("password wrong!");
  						return;
  					}
  					window.location.href="${pageContext.request.contextPath }/admin/index.jsp";
  				}
  			});
  		});
  	});
  	
  </script>
</html>
