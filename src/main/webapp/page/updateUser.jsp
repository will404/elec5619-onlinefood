<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon"
	href="${pageContext.request.contextPath }/admin/img/favicon.png">
<title>Index</title>
<link
	href="${pageContext.request.contextPath }/admin/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath }/admin/css/bootstrap-theme.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath }/admin/css/elegant-icons-style.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath }/admin/css/font-awesome.min.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath }/admin/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath }/admin/assets/fullcalendar/fullcalendar/fullcalendar.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath }/admin/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css"
	rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath }/admin/css/owl.carousel.css"
	type="text/css">
<link
	href="${pageContext.request.contextPath }/admin/css/jquery-jvectormap-1.2.2.css"
	rel="stylesheet">
<link rel="stylesheet"
	href="${pageContext.request.contextPath }/admin/css/fullcalendar.css">
<link href="${pageContext.request.contextPath }/admin/css/widgets.css"
	rel="stylesheet">
<link href="${pageContext.request.contextPath }/admin/css/style.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath }/admin/css/style-responsive.css"
	rel="stylesheet" />
<link
	href="${pageContext.request.contextPath }/admin/css/xcharts.min.css"
	rel=" stylesheet">
<link
	href="${pageContext.request.contextPath }/admin/css/jquery-ui-1.10.4.min.css"
	rel="stylesheet">
</head>

<body ng-app="app" ng-controller="udpateCtrl">
	<section id="container" class="">
		<!--main content start-->
		<section id="main-content">
			<section class="wrapper">
				<div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                             Personal information
                          </header>
                          <div class="panel-body">
                              <form class="form-horizontal " id="formId" method="post">
                                
                                  <div class="form-group">
                                      <label class="col-sm-2 control-label">Login Name</label>
                                      <div class="col-sm-10">
                                          <input class="form-control" disabled="disabled" ng-model="loginName" type="text"  >
                                      </div>
                                  </div>
                                    <div class="form-group">
                                      <label class="col-sm-2 control-label">password</label>
                                      <div class="col-sm-10">
                                          <input class="form-control" ng-model="loginPwd" type="password"  >
                                      </div>
                                  </div>
                                  
                                  <div class="form-group">
                                      <label class="col-sm-2 control-label">Display Name</label>
                                      <div class="col-sm-10">
                                          <input class="form-control" ng-model="displayName" type="text"  >
                                      </div>
                                  </div>
                                   <div class="form-group">
                                      <label class="col-sm-2 control-label">Email</label>
                                      <div class="col-sm-10">
                                          <input class="form-control"  ng-model="email" type="text"  >
                                      </div>
                                  </div>
                                   <div class="form-group">
                                      <label class="col-sm-2 control-label">Phone</label>
                                      <div class="col-sm-10">
                                          <input class="form-control onlynumber"  ng-model="phone" type="text"  >
                                      </div>
                                  </div>
                                   <div class="form-group">
                                      <label class="col-sm-2 control-label">Address</label>
                                      <div class="col-sm-10">
                                          <input class="form-control" ng-model="address" type="text"  >
                                      </div>
                                  </div>
                                   <span class="btn btn-primary" ng-click="update()">Save</span>
                              </form>
                          </div>
                      </section>
                  </div>
			</section>
		</section>
		<!--main content end-->
	</section>
	<!-- container section start -->

	<!-- javascripts -->
	<script src="${pageContext.request.contextPath }/admin/js/jquery.js"></script>
	<script
		src="${pageContext.request.contextPath }/admin/js/bootstrap.min.js"></script>
	<!-- nice scroll -->
	<script
		src="${pageContext.request.contextPath }/admin/js/jquery.scrollTo.min.js"></script>
	<script
		src="${pageContext.request.contextPath }/admin/js/jquery.nicescroll.js"
		type="text/javascript"></script>
	<!-- jquery validate js -->
	<script type="text/javascript"
		src="${pageContext.request.contextPath }/admin/js/jquery.validate.min.js"></script>

	<!-- custom form validation script for this page-->
	<script
		src="${pageContext.request.contextPath }/admin/js/form-validation-script.js"></script>
	<!--custome script for all page-->
	<script src="${pageContext.request.contextPath }/admin/js/scripts.js"></script>
	<script src="${pageContext.request.contextPath }/common/js/angular.1.5.5.min.js"></script>

</body>
<script type="text/javascript">
function isNumber(keyCode,flag) {
	if (keyCode >= 48 && keyCode <= 57 ) 
		return true;
	if (keyCode >= 96 && keyCode <= 105) 
		return true;
	if(flag){
		if(keyCode == 110 || keyCode == 190) 
			return true;
	}
	if (keyCode == 8) 
		return true;
	return false;
} 

$(function(){
	$(".onlynumber").keydown(function(event){
		return isNumber(event.keyCode,false);
	});
	$(".price").keydown(function(event){
		return isNumber(event.keyCode,true);
	});
});

</script>
<script type="text/javascript">
	var app = angular.module("app",[]);
	app.controller("udpateCtrl",function($scope,$http){
		$http.post("${pageContext.request.contextPath}/user/getUser.action").then(function(result){
			
			$scope.user = result.data;
			$scope.loginName = $scope.user.loginName;
			$scope.displayName= $scope.user.displayName;
			$scope.email= $scope.user.email;
			$scope.phone= $scope.user.phone;
			$scope.address= $scope.user.address;
		});
		$scope.update = function() {
			if($scope.loginName == ""||$scope.loginName ==null) {
				alert("please input Login Name");
				return;
			}
			if($scope.loginPwd == ""||$scope.loginPwd ==null) {
				alert("please input login Password");
				return;
			}
			if($scope.loginPwd == ""||$scope.loginPwd ==null) {
				alert("please input login Password");
				return;
			}
			if($scope.displayName == ""||$scope.displayName ==null) {
				alert("please input displayName");
				return;
			}
			if($scope.loginPwd == ""||$scope.loginPwd ==null) {
				alert("please input login Password");
				return;
			}
			if($scope.email == ""||$scope.email ==null) {
				alert("please input email");
				return;
			}
			
			var rex = /^[a-z0-9]+([._\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$/;
			if($scope.email !=""){
				if(!rex.test($scope.email)){
					alert("check your email");
					return;
				}
			}
			if($scope.phone == ""||$scope.phone ==null) {
				alert("please input phone");
				return;
			}
			if($scope.address == ""||$scope.address ==null) {
				alert("please input address");
				return;
			}
			var url = "${pageContext.request.contextPath}/user/updateUsers.action";
			 $.ajax({
		    		type : "post", 
					url : url,
					data :{
						"loginName":$scope.loginName,
						"loginPwd":$scope.loginPwd,
						"displayName":$scope.displayName,
						"email":$scope.email,
						"phone":$scope.phone,
						"address":$scope.address
					},
					success : function(data) {
						if(data == 1) {
							alert("Update Success");
						}else{
							alert("Updata fail");
						}
						
					}
		    	});
			
		}
	});
</script>
</html>
