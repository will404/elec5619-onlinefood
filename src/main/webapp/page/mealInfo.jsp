<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<!DOCTYPE html>
<html>
<head>
<title>single</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //Custom Theme files -->
<link href="${pageContext.request.contextPath }/page/css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
<link href="${pageContext.request.contextPath }/page/css/style.css" type="text/css" rel="stylesheet" media="all">
<!-- js -->
<script src="${pageContext.request.contextPath }/page/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/page/js/bootstrap-3.1.1.min.js"></script>
<script src="${pageContext.request.contextPath }/page/js/imagezoom.js"></script>
<!-- //js -->
<!-- cart -->
<script src="${pageContext.request.contextPath }/page/js/simpleCart.min.js"> </script>
<!-- cart -->
<!-- FlexSlider -->
<script defer src="${pageContext.request.contextPath }/page/js/jquery.flexslider.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath }/page/css/flexslider.css" type="text/css" media="screen" />
<script>
	// Can also be used with $(document).ready()
	$(window).load(function() {
	  $('.flexslider').flexslider({
		animation: "slide",
		controlNav: "thumbnails"
	  });
	});
</script>
<!--//FlexSlider -->
</head>
<body ng-app="app" ng-controller="pageCtrl">
	<!--header-->
	<div class="header">
		<div class="container">
			<nav class="navbar navbar-default" role="navigation">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<h1 class="navbar-brand"><a  href="${pageContext.request.contextPath}/page/index.jsp">OnlineFood</a></h1>
				</div>
				<!--navbar-header-->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li><a href="${pageContext.request.contextPath}/page/index.jsp">Home</a></li>
						<li class="dropdown grid">
							<a href="#" class="dropdown-toggle list1" data-toggle="dropdown">Meal Series<b class="caret"></b></a>
							<ul class="dropdown-menu multi-column columns-3">
								<div class="row">
									<div class="col-sm-4 col-md-4">
										<ul class="multi-column-dropdown">
											<li><a class="list" href="${pageContext.request.contextPath}/page/mealSeries.jsp">All</a></li>
											<li><a class="list" href="${pageContext.request.contextPath}/page/mealSeries.jsp?seriesId={{i.id}}" ng-repeat="i in ms">{{i.seriesName}}</a></li>
										</ul>
									</div>
								</div>
							</ul>
						</li>								
					</ul> 
					<!--/.navbar-collapse-->
				</div>
				<!--//navbar-header-->
			</nav>
			<div class="header-info">
				<div class="header-right search-box">
					<a href="#"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a>				
					<div class="search">
						<form class="navbar-form">
							<input ng-model="keyword" type="text" class="form-control">
							<button type="submit" ng-click="search()" class="btn btn-default" aria-label="Left Align">
								Search
							</button>
						</form>
					</div>	
				</div>
				<div class="header-right login">
					<a href="#"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></a>
					<c:if test="${user == null}">
					<div id="loginBox">     
							<form id="loginForm">
								<fieldset id="body">
									<fieldset>
										<label for="email">Login Name</label>
										<input type="text" ng-model="loginName" name="loginName"/>
									</fieldset>
									<fieldset>
										<label for="password">Password</label>
										<input type="password" ng-model="loginPwd" name="password">
									</fieldset>
									<span class="btn" id="login">Sign in</span>
								</fieldset>
								<p>New User ? <a class="sign" href="${pageContext.request.contextPath }/page/sign-up.jsp">Sign Up</a></p>
							</form>
						
				      	    
						
					</div>
				      	</c:if>     
				      	
				      	<c:if test="${user != null}">
				      	<div id="loginBox"> 
							<form id="loginForm" class="navbar-form">
								<fieldset id="body">
									<fieldset>
										<span>Welcome:${user.displayName }</span>
									</fieldset>
									<fieldset>
										<a class="btn btn-default" ng-click="update_user()">profile</a>
									</fieldset>
									<fieldset>
										<a class="btn btn-default" href="${pageContext.request.contextPath }/page/orders.jsp">my orders</a>
									</fieldset>
									<fieldset>
										<a class="btn btn-default" href="${pageContext.request.contextPath }/user/logoutUsers.action">Logout</a>
									</fieldset>
									
								</fieldset>
							</form>
						</div>
				      	</c:if>       
				</div>
				<div class="header-right cart">
					<a href="#"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span></a>
					<div class="cart-box">
						<c:if test="${user != null}">
							<h4><a href="${pageContext.request.contextPath }/page/cart.jsp">
									<span> <span>$</span>${user.cart.cartPriceDouble } </span> (<span> ${fn:length(user.cart.cartItemSet)} </span>) 
								</a></h4>
							<p><a href="${pageContext.request.contextPath }/page/cart.jsp" class="simpleCart_empty">view cart</a></p>
							<div class="clearfix"> </div>
						</c:if>
						
						<c:if test="${user == null}">
							<h4><a href="#">
								<span > $0.00 </span> (<span> 0 </span>) 
							</a></h4>
							<p><a href="javascript:;" class="simpleCart_empty">Empty cart</a></p>
							<div class="clearfix"> </div>
						</c:if> 
					</div>
				</div>
				
				<div class="clearfix"> </div>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<!--//header-->
	<!--//single-page-->
	<div class="single">
		<div class="container">
			<div class="single-grids">				
				<div class="col-md-6 single-grid">		
					<div class="flexslider">
								<div class="thumb-image"> <img src="{{meal.mealImage}}" data-imagezoom="true" class="img-responsive"> </div>
					</div>
				</div>	
				<div class="col-md-4 single-grid simpleCart_shelfItem">		
					<h3>{{meal.mealName}}</h3>
					<p>{{meal.mealSummarize}}</p>
					<div class="galry">
						<div class="prices">
							<h5 class="item_price"><span>$</span>{{meal.mealPriceDouble}}</h5>
						</div>
						<div class="rating">
							<div class="rating">
								<span ng-repeat="i in range((5-meal.rating))">☆</span>
								<span style="color: #F07818" ng-repeat="i in range(meal.rating)">★</span>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<p class="qty"> Qty :  </p><input min="1" type="number" ng-init="count='1'" id="quantity" ng-model="count" name="quantity" value="1" class="form-control onlynumber input-small">
					<div class="btn_form">
						<a href="#" ng-click="addCart()" class="add-cart  item_add">ADD TO CART</a>	
					</div>
					<div class="tag">
						<p>Category : <a href="${pageContext.request.contextPath}/page/mealSeries.jsp?seriesId={{meal.mealSeries.id}}">{{meal.mealSeries.seriesName}}</a></p>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!-- collapse -->
	<div class="collpse tabs">
		<div class="container">
			<div class="panel-group collpse" id="accordion" role="tablist" aria-multiselectable="true">
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingOne">
						<h4 class="panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
							  Description
							</a>
						</h4>
					</div>
					<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
						<div class="panel-body">
								<div style="word-wrap:break-word;word-break:break-all;">{{meal.mealDescription}}</div>
						</div>
					</div>
				</div>
				
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingThree">
						<h4 class="panel-title">
							<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
								reviews ({{allViews}})
							</a>
						</h4>
					</div>
					<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
						<div class="panel-body" ng-repeat="c in comments">
							<h4>user:&nbsp;&nbsp;{{c.user.loginName}}&nbsp;&nbsp;&nbsp;&nbsp;
								<!-- <span style="color: #F07818" ng-repeat="i in range(c.rating)">★</span>
								<span ng-repeat="i in range((5-c.rating))">☆</span> -->
								<span style="color: #F07818" ng-repeat="i in range(5)">
									<span ng-if="$index <= c.rating">★</span>
									<span ng-if="$index > c.rating">☆</span>
								</span>
								&nbsp;&nbsp;&nbsp;&nbsp;<span>{{c.commentTimeStr}}</span>
							</h4>
							{{c.comment}}
						</div>
						<div class="col-md-12 text-center">
							cur:<span>{{curPage}}</span>/<span>{{allPage}}</span> &nbsp;&nbsp;
												<a href="#" ng-click="init_page_data('H')">Head</a>&nbsp;
												 <a href="#" ng-click="init_page_data('P')">Pre</a>&nbsp;
												 <a href="#" ng-click="init_page_data('N')">Next</a>&nbsp;
												 <a href="#" ng-click="init_page_data('T')"> Tail</a>
						</div>
					</div>
					
				</div>
				
				
			</div>
		</div>
	</div>
	<!--//collapse -->
	<!--//footer-->
	<div class="footer-bottom">
		<div class="container">
			<p>Copyright &copy; 2017.Online food order app.</p>
		</div>
	</div>
</body>
<script src="${pageContext.request.contextPath }/common/js/angular.1.5.5.min.js"></script>
<script src="${pageContext.request.contextPath }/common/layer/layer.js"></script>
<script type="text/javascript">
	function getJson(length) {
		var json=new Array(length);
		for(var i = 0;i<length;i++) {
			json[i] = i;
		}
		return json;
	}
</script>

<script type="text/javascript">
function isNumber(keyCode,flag) {
	
	// 数字
	if (keyCode >= 48 && keyCode <= 57 ) 
		return true;
	// 小数字键盘
	if (keyCode >= 96 && keyCode <= 105) 
		return true;
	if(flag){
		if(keyCode == 110 || keyCode == 190) 
			return true;
	}

	// Backspace键
	if (keyCode == 8) 
		return true;
	return false;
} 

$(function(){
	$(".onlynumber").keydown(function(event){
		return isNumber(event.keyCode,false);
	});
});
</script>
<script type="text/javascript">
	var app = angular.module("app",[]);
	app.controller("pageCtrl",function($scope,$http){
		var mealId = document.URL.split("?")[1].split("=")[1];
		var mealUrl = "${pageContext.request.contextPath }/meal/getMeal.action?id="+mealId;
		$http.post(mealUrl).then(function(result){
			$scope.meal = result.data;
		});	

		$http.post("${pageContext.request.contextPath }/mealSeries/findMSAll.action").then(function(result){
			$scope.ms = result.data;
		});	
		$scope.curPage = 1;
		$scope.init_page = function() {
			var url = "${pageContext.request.contextPath}/comment/countComment.action?mealId="+mealId;
			$http.get(url).then(function(result){
				$scope.allPage = result.data;
			});
			
			url = "${pageContext.request.contextPath}/comment/countCommentAll.action?mealId="+mealId;
			$http.get(url).then(function(result){
				$scope.allViews = result.data;
			});
		}
		
		$scope.update_user = function() {
			layer.open({
				  type: 2,
				  area: ["700px", "450px"],
				  fixed: false, 
				  maxmin: true,
				  title:"Update user",
				  cancel: function(index, layero){ 
					    layer.close(index);
					    window.location.reload();
					},
				  content: "${pageContext.request.contextPath }/page/updateUser.jsp"
				});
		}
		$scope.init_page_data = function(option)	{
			if(option == "H"){
				$scope.curPage = 1;
			}
			if(option == "T"){
				$scope.curPage = $scope.allPage;
			}
			if(option == "N") {
		
				if($scope.curPage <  $scope.allPage){
					++$scope.curPage;
				}
			}
			if(option == "P") {
				if($scope.curPage - 1 >= 1){
					--$scope.curPage;
				}
			}
			var url = "${pageContext.request.contextPath}/comment/getPageCommentByMeal.action?page="+$scope.curPage+"&mealId="+mealId;
			  $http.post(url)
			    .then(function (result) {
			        $scope.comments = result.data;
			    });
		};		
		
		
		$scope.range = function(length) {
			var json=new Array(length);
			for(var i = 0;i<length;i++) {
				json[i] = i;
			}
			return json;
		}
		$scope.search = function() {
			if($scope.keyword == null || $scope.keyword=="") {
				alert("please input keyword you want to search");
				return;
			}
			window.location.href="${pageContext.request.contextPath}/page/mealSeries.jsp?keyword="+$scope.keyword;
		};
		$scope.addCart = function() {
			var url = "${pageContext.request.contextPath}/meal/addToCart.action";
			if($scope.count=="" || $scope.count== null) {
				alert("please input the count");
				return;
			}
			var count = parseInt($scope.count);
			if(count <= 0) {
				alert("please input the count > 0");
				return;
			}
			$.ajax({
		    		type : "post", 
					url : url,
					data :{
						"mealId":$scope.meal.id,
						"count":count
					},
					success : function(data) {
						if(data == 1) {
							alert("add to cart success");
							window.location.reload();
						}else{
							alert("please login first");
						}
					}
		    	}); 
		};
		$("#login").click( function () {
			if($scope.loginName == ""||$scope.loginName ==null) {
				alert("please input Login Name");
				return;
			}
			if($scope.loginPwd == ""||$scope.loginPwd ==null) {
				alert("please input login Password");
				return;
			}
			var url = "${pageContext.request.contextPath}/user/loginUsers.action";
			 $.ajax({
		    		type : "post", 
					url : url,
					data :{
						"loginName":$scope.loginName,
						"loginPwd":$scope.loginPwd
					},
					success : function(data) {
						if(data == 0) {
							alert("user isn't exist or password wrong");
							return;
						}
						window.location.reload();
					}
		    	});
		}); 
		$scope.init_page();
		$scope.init_page_data(0);
	});
</script>
</html>