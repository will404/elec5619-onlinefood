<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<!DOCTYPE html>
<html>
<head>
<title>Home</title>
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //Custom Theme files -->
<link href="${pageContext.request.contextPath }/page/css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
<link href="${pageContext.request.contextPath }/page/css/style.css" type="text/css" rel="stylesheet" media="all">
<!-- js -->
<script src="${pageContext.request.contextPath }/page/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/page/js/bootstrap-3.1.1.min.js"></script>
<!-- //js -->	
<!-- cart -->
<script src="${pageContext.request.contextPath }/page/js/simpleCart.min.js"> </script>
<!-- cart -->
</head>
<body ng-app="app" ng-controller="pageCtrl">
<h4 class="text-center">meal name: {{oi.meal.mealName}}</h4>
<form class="form-horizontal">
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Comment</label>
    <div class="col-sm-10">
      <textarea class="form-control" ng-model="str" rows="3"></textarea>
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Ration</label>
    <div class="col-sm-10">
      <select class="form-control" ng-model="rating" ng-init="rating='1'">
      	<option vallue="1">1</option>
      	<option vallue="2">2</option>
      	<option vallue="3">3</option>
      	<option vallue="4">4</option>
      	<option vallue="5">5</option>
      </select>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button ng-click="add()" class="btn btn-default">submit</button>
    </div>
  </div>
  </form>
</body>
<script src="${pageContext.request.contextPath }/common/js/angular.1.5.5.min.js"></script>
<script type="text/javascript">
	var app = angular.module("app",[]);
	app.controller("pageCtrl",function($scope,$http){
		var id = document.URL.split("?")[1].split("=")[1];
		var url = "${pageContext.request.contextPath }/orders/getOrderItem.action?id="+id;
		$http.post(url).then(function(result){
			$scope.oi = result.data;
		});	
		
		$scope.add=function(){
			if($scope.str == null || $scope.str=="") {
				alert("please input comment");
				return;
			}
			$.ajax({
				type : "post",
				url : "${pageContext.request.contextPath }/comment/addComment.action",
				data : {
					"str":$scope.str,
					"rating":$scope.rating,
					"ordersItemId":id
				},
				success : function(data) {
					if (data == 0) {
						alert("user isn't exist or password wrong");
						return;
					}else if(data ==1) {
						alert("comment success");
					}else if(data ==2) {
						alert("you had already comment!");
					}
					
				}
			});
		}
	});
</script>

</html>