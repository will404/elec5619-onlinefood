<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<!DOCTYPE html>
<html>
<head>
<title>Home</title>
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //Custom Theme files -->
<link href="${pageContext.request.contextPath }/page/css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
<link href="${pageContext.request.contextPath }/page/css/style.css" type="text/css" rel="stylesheet" media="all">
<!-- js -->
<script src="${pageContext.request.contextPath }/page/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/page/js/bootstrap-3.1.1.min.js"></script>
<!-- //js -->	
<!-- cart -->
<script src="${pageContext.request.contextPath }/page/js/simpleCart.min.js"> </script>
<!-- cart -->

</head>
<body ng-app="app" ng-controller="pageCtrl">
		<!--header-->
	<div class="header">
		<div class="container">
			<nav class="navbar navbar-default" role="navigation">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<h1 class="navbar-brand"><a  href="${pageContext.request.contextPath}/page/index.jsp">OnlineFood</a></h1>
				</div>
				<!--navbar-header-->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li><a href="${pageContext.request.contextPath}/page/index.jsp">Home</a></li>
						<li class="dropdown grid">
							<a href="#" class="active dropdown-toggle list1" data-toggle="dropdown">Meal Series<b class="caret"></b></a>
							<ul class="dropdown-menu multi-column columns-3">
								<div class="row">
									<div class="col-sm-4 col-md-4">
										<ul class="multi-column-dropdown">
											<li><a class="list" href="${pageContext.request.contextPath}/page/mealSeries.jsp">All</a></li>
											<li><a class="list" href="${pageContext.request.contextPath}/page/mealSeries.jsp?seriesId={{i.id}}" ng-repeat="i in ms">{{i.seriesName}}</a></li>
										</ul>
									</div>
								</div>
							</ul>
						</li>								
					</ul> 
					<!--/.navbar-collapse-->
				</div>
				<!--//navbar-header-->
			</nav>
			<div class="header-info">
				<div class="header-right search-box">
					<a href="#"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a>				
					<div class="search">
						<form class="navbar-form">
							<input ng-model="keyword" type="text" class="form-control">
							<button type="submit" ng-click="search()" class="btn btn-default" aria-label="Left Align">
								Search
							</button>
						</form>
					</div>	
				</div>
				<div class="header-right login">
					<a href="#"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></a>
					<c:if test="${user == null}">
					<div id="loginBox">     
						
							<form id="loginForm">
								<fieldset id="body">
									<fieldset>
										<label for="email">Login Name</label>
										<input type="text" ng-model="loginName" name="loginName"/>
									</fieldset>
									<fieldset>
										<label for="password">Password</label>
										<input type="password" ng-model="loginPwd" name="password">
									</fieldset>
									<span class="btn" id="login">Sign in</span>
								</fieldset>
								<p>New User ? <a class="sign" href="${pageContext.request.contextPath }/page/sign-up.jsp">Sign Up</a></p>
							</form>
						
				      	    
						
					</div>
				      	</c:if>     
				      	
				      	<c:if test="${user != null}">
				      	<div id="loginBox"> 
							<form id="loginForm">
								<fieldset id="body">
									<fieldset>
										<span>Welcome:${user.displayName }</span>
									</fieldset>
									<fieldset>
										<a class="btn btn-default" ng-click="update_user()">profile</a>
									</fieldset>
									<fieldset>
										<a class="btn btn-default" href="${pageContext.request.contextPath }/page/orders.jsp">my orders</a>
									</fieldset>
									<fieldset>
										<a class="btn btn-default" href="${pageContext.request.contextPath }/user/logoutUsers.action">Logout</a>
									</fieldset>
									
								</fieldset>
							</form>
						</div>
				      	</c:if>       
				</div>
				<div class="header-right cart">
					<a href="#"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span></a>
					<div class="cart-box">
						<c:if test="${user != null}">
							<h4><a href="${pageContext.request.contextPath }/page/cart.jsp">
									<span> <span>$</span>${user.cart.cartPriceDouble } </span> (<span> ${fn:length(user.cart.cartItemSet)} </span>) 
								</a></h4>
							<p><a href="${pageContext.request.contextPath }/page/cart.jsp" class="simpleCart_empty">view cart</a></p>
							<div class="clearfix"> </div>
						</c:if>
						
						<c:if test="${user == null}">
							<h4><a href="#">
								<span> $0.00 </span> (<span> 0 </span>) 
							</a></h4>
							<p><a href="javascript:;" class="simpleCart_empty">Empty cart</a></p>
							<div class="clearfix"> </div>
						</c:if> 
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<!--//header-->
	<!--products-->
	<div class="products">	 
		<div class="container">
			<h2>Our Products</h2>		
				
			<div class="col-md-12 product-model-sec">
				<div class="product-grid col-md-4" ng-repeat="m in meallist">
					<a href="${pageContext.request.contextPath }/page/mealInfo.jsp?id={{m.id}}">
						<div class="more-product"><span> </span></div>						
						<div class="product-img b-link-stripe b-animate-go  thickbox">
							<img width="329.8px" height="273.8px" src="{{m.mealImage}}" alt="image miss"/>
							<div class="b-wrapper">
							<h4 class="b-animate b-from-left  b-delay03">							
							<button> View </button>
							</h4>
							</div>
						</div>
					</a>				
					<div class="product-info simpleCart_shelfItem">
						<div class="product-info-cust prt_name">
							<h4>{{m.mealName}}</h4>								
							<span class="item_price"><span>$</span>{{m.mealPrice/100}}</span>
							<input type="text" class="item_quantity onlynumber" value="1" />
							<input type="button" class="item_add items" value="Add" ng-click="addCart($event,m.id)"/>
							<div class="clearfix"> </div>
						</div>						
					</div>
				</div>
			</div>	
			<div class="col-md-12 text-center">
				cur:<span>{{curPage}}</span>/<span>{{allPage}}</span> &nbsp;&nbsp;
									<a href="#" ng-click="init_page_data('H')">Head</a>&nbsp;
									 <a href="#" ng-click="init_page_data('P')">Pre</a>&nbsp;
									 <a href="#" ng-click="init_page_data('N')">Next</a>&nbsp;
									 <a href="#" ng-click="init_page_data('T')"> Tail</a>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<!--//products-->
	
	<!--//footer-->
	<div class="footer-bottom">
		<div class="container">
			<p>Copyright &copy; 2017.Online food order app.</p>
		</div>
	</div>
</body>
<script src="${pageContext.request.contextPath }/common/js/angular.1.5.5.min.js"></script>
<script src="${pageContext.request.contextPath }/common/layer/layer.js"></script>
<script type="text/javascript">
	function getJson(length) {
		var json=new Array(length);
		for(var i = 0;i<length;i++) {
			json[i] = i;
		}
		return json;
	}
</script>


<script type="text/javascript">
	var app = angular.module("app",[]);
	app.controller("pageCtrl",function($scope,$http){
		$scope.curPage = 1;
		var keyword = null;
		var op = 0;
		var url = document.URL;
		var seriesId = 0;
		var pageUrl = "${pageContext.request.contextPath}/meal/countConditionMealPage.action?";
		var dataUrl = "${pageContext.request.contextPath}/meal/findMealByCondition.action?";
		if(url.indexOf("keyword") >= 0 ) {
			//alert(url.substring(url.lastIndexOf("=") + 1,url.length));
			keyword = url.split("?")[1].split("=")[1];
			op = 2;
			
		} 
		
		if(url.indexOf("seriesId") >= 0 ) {
			seriesId = url.split("?")[1].split("=")[1];
			op = 1;
		} 
		$http.post("${pageContext.request.contextPath }/mealSeries/findMSAll.action").then(function(result){
			$scope.ms = result.data;
		});	
		$scope.update_user = function() {
			layer.open({
				  type: 2,
				  area: ["700px", "450px"],
				  fixed: false, 
				  maxmin: true,
				  title:"Update user",
				  cancel: function(index, layero){ 
					    layer.close(index);
					    window.location.reload();
					},
				  content: "${pageContext.request.contextPath }/page/updateUser.jsp"
				});
		}
		$scope.init_page = function() {
			if(op == 0) {
				pageUrl+="op="+op;
				dataUrl+="op="+op;
			}else if(op == 1) {
				pageUrl+="op="+op+"&seriesId="+seriesId;
				dataUrl+="op="+op+"&seriesId="+seriesId;
			}else if(op ==2) {
				pageUrl+="op="+op+"&keyword="+keyword;
				dataUrl+="op="+op+"&keyword="+keyword;
			}
			$http.get(pageUrl).then(function(result){
				$scope.allPage = result.data;
			});
		}
		$scope.init_page_data = function(option)	{
			if(option == "H"){
				$scope.curPage = 1;
			}
			if(option == "T"){
				$scope.curPage = $scope.allPage;
			}
			if(option == "N") {
		
				if($scope.curPage <  $scope.allPage){
					++$scope.curPage;
				}
			}
			if(option == "P") {
				if($scope.curPage - 1 >= 1){
					--$scope.curPage;
				}
			}
		
			var url = dataUrl+"&page="+$scope.curPage;
			  $http.post(url)
			    .then(function (result) {
			        $scope.meallist = result.data;
			    });
		};		
		
		$("#login").click( function () {
			if($scope.loginName == ""||$scope.loginName ==null) {
				alert("please input Login Name");
				return;
			}
			if($scope.loginPwd == ""||$scope.loginPwd ==null) {
				alert("please input login Password");
				return;
			}
			var url = "${pageContext.request.contextPath}/user/loginUsers.action";
			 $.ajax({
		    		type : "post", 
					url : url,
					data :{
						"loginName":$scope.loginName,
						"loginPwd":$scope.loginPwd
					},
					success : function(data) {
						if(data == 0) {
							alert("user isn't exist or password wrong");
							return;
						}
						window.location.reload();
					}
		    	});
		}); 
		$scope.search = function() {
			if($scope.keyword == null || $scope.keyword=="") {
				alert("please input keyword you want to search");
				return;
			}
			window.location.href="${pageContext.request.contextPath}/page/mealSeries.jsp?keyword="+$scope.keyword;
		};
		$scope.addCart = function(event,mealId) {
			var url = "${pageContext.request.contextPath}/meal/addToCart.action";
			var count = parseInt($(event.target).prev().val());
			if(count <= 0) {
				alert("please input the count > 0");
				return;
			}
			if(isNaN(count)) {
				alert("please input the count");
				return;
			}
			$.ajax({
		    		type : "post", 
					url : url,
					data :{
						"mealId":mealId,
						"count":count
					},
					success : function(data) {
						if(data == 1) {
							alert("add to cart success");
							window.location.reload();
						}else{
							alert("please login first");
						}
					}
		    	}); 
		};
		$scope.init_page();
		$scope.init_page_data(0);
	});
</script>
</html>