package com.elec5619.orderapp.dao;

import com.elec5619.orderapp.base.dao.BaseDao;
import com.elec5619.orderapp.domain.Users;

public interface UsersDao extends BaseDao<Users>{

}
