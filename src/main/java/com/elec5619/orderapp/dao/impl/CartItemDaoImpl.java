package com.elec5619.orderapp.dao.impl;

import com.elec5619.orderapp.base.dao.impl.BaseDaoImpl;
import com.elec5619.orderapp.dao.CartItemDao;
import com.elec5619.orderapp.domain.CartItem;

public class CartItemDaoImpl  extends BaseDaoImpl<CartItem> implements CartItemDao{

	@Override
	protected Class<?> getEntityType() {
		// TODO Auto-generated method stub
		return CartItem.class;
	}

}