package com.elec5619.orderapp.dao.impl;

import org.springframework.stereotype.Repository;

import com.elec5619.orderapp.base.dao.impl.BaseDaoImpl;
import com.elec5619.orderapp.dao.MealDao;
import com.elec5619.orderapp.domain.Meal;

@Repository
public class MealDaoImpl extends BaseDaoImpl<Meal> implements MealDao{

	@Override
	protected Class<?> getEntityType() {
		// TODO Auto-generated method stub
		return Meal.class;
	}

}
