package com.elec5619.orderapp.dao.impl;

import com.elec5619.orderapp.base.dao.impl.BaseDaoImpl;
import com.elec5619.orderapp.dao.CartDao;
import com.elec5619.orderapp.domain.Cart;

public class CartDaoImpl extends BaseDaoImpl<Cart> implements CartDao{

	@Override
	protected Class<?> getEntityType() {
		// TODO Auto-generated method stub
		return Cart.class;
	}

}
