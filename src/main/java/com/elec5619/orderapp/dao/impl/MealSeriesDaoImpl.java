package com.elec5619.orderapp.dao.impl;

import org.springframework.stereotype.Repository;

import com.elec5619.orderapp.base.dao.impl.BaseDaoImpl;
import com.elec5619.orderapp.dao.MealSeriesDao;
import com.elec5619.orderapp.domain.MealSeries;
@Repository
public class MealSeriesDaoImpl extends BaseDaoImpl<MealSeries> implements MealSeriesDao{

	@Override
	protected Class<?> getEntityType() {
		// TODO Auto-generated method stub
		return MealSeries.class;
	}

}
