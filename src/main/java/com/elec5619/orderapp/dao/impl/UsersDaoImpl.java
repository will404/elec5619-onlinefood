package com.elec5619.orderapp.dao.impl;

import org.springframework.stereotype.Repository;

import com.elec5619.orderapp.base.dao.impl.BaseDaoImpl;
import com.elec5619.orderapp.dao.UsersDao;
import com.elec5619.orderapp.domain.Users;
@Repository
public class UsersDaoImpl extends BaseDaoImpl<Users> implements UsersDao{

	@Override
	protected Class<?> getEntityType() {
		// TODO Auto-generated method stub
		return Users.class;
	}

}
