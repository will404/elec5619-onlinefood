package com.elec5619.orderapp.dao.impl;

import org.springframework.stereotype.Repository;

import com.elec5619.orderapp.base.dao.impl.BaseDaoImpl;
import com.elec5619.orderapp.dao.OrdersDao;
import com.elec5619.orderapp.domain.Orders;
@Repository
public class OrdersDaoImpl extends BaseDaoImpl<Orders> implements OrdersDao{

	@Override
	protected Class<?> getEntityType() {
		// TODO Auto-generated method stub
		return Orders.class;
	}

}
