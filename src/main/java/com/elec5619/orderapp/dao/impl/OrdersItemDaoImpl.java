package com.elec5619.orderapp.dao.impl;

import org.springframework.stereotype.Repository;

import com.elec5619.orderapp.base.dao.impl.BaseDaoImpl;
import com.elec5619.orderapp.dao.OrdersItemDao;
import com.elec5619.orderapp.domain.OrdersItem;
@Repository
public class OrdersItemDaoImpl extends BaseDaoImpl<OrdersItem> implements OrdersItemDao{

	@Override
	protected Class<?> getEntityType() {
		// TODO Auto-generated method stub
		return OrdersItem.class;
	}

}
