package com.elec5619.orderapp.dao.impl;

import com.elec5619.orderapp.base.dao.impl.BaseDaoImpl;
import com.elec5619.orderapp.dao.CommentDao;
import com.elec5619.orderapp.domain.Comment;

public class CommentDaoImpl  extends BaseDaoImpl<Comment> implements CommentDao{

	@Override
	protected Class<?> getEntityType() {
		// TODO Auto-generated method stub
		return Comment.class;
	}

}