package com.elec5619.orderapp.dao;

import com.elec5619.orderapp.base.dao.BaseDao;
import com.elec5619.orderapp.domain.Cart;

public interface CartDao extends BaseDao<Cart>{

}