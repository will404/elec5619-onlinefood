package com.elec5619.orderapp.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.elec5619.orderapp.constant.Constant;
import com.elec5619.orderapp.domain.Cart;
import com.elec5619.orderapp.domain.CartItem;
import com.elec5619.orderapp.domain.Meal;
import com.elec5619.orderapp.domain.Users;
import com.elec5619.orderapp.service.CartItemService;
import com.elec5619.orderapp.service.CartService;
import com.elec5619.orderapp.service.MealSeriesService;
import com.elec5619.orderapp.service.MealService;
import com.elec5619.orderapp.service.UsersService;
import com.elec5619.orderapp.utils.FileUtils;

@Controller
@RequestMapping("/meal")
public class MealController {

	@Autowired
	private MealService mealService;
	@Autowired
	private UsersService usersService;
	@Autowired
	private MealSeriesService mealSeriesService;
	@Autowired
	private CartService cartService;
	@Autowired
	private CartItemService cartItemService;
	@RequestMapping("/addMeal")
	@ResponseBody
	public String addMeal(@RequestParam(value = "file", required = false) MultipartFile file,Meal meal,int seriesId,HttpServletRequest request) {
		String img = FileUtils.saveFile(request, file);
		
		meal.setMealImage(img);
		meal.setMealSeries(mealSeriesService.getEntityById(seriesId));
		mealService.saveOrUpdate(meal);
		return Constant.SUCCESS.toString();
	}
	
	// delete meal
	@RequestMapping("/deleteMeal")
	@ResponseBody
	public String deleteMeal(int id,HttpServletRequest request) {
		if(mealService.hasOrder(id).equals(Constant.MEAL_HAS_ORDER.toString())) {
			return Constant.MEAL_HAS_ORDER.toString();
		}
		Meal meal = mealService.getEntityById(id);
		FileUtils.deleteFile(request, meal.getMealImage());
		mealService.deleteEntityByIds(id);
		
		return Constant.SUCCESS.toString();
		
	}
	// update meal
	@RequestMapping("/updateMeal")
	@ResponseBody
	public String updateMeal(@RequestParam(value = "file", required = false) MultipartFile file,int seriesId,Meal meal,HttpServletRequest request) {
		String img = "";
		if(file != null) {
			img = FileUtils.saveFile(request, file);
		}
		System.out.println("new  "+img);
		Meal oldMeal = mealService.getEntityById(meal.getId());
		
		oldMeal.setMealDescription(meal.getMealDescription());
		if(!img.equals("")) {
			FileUtils.deleteFile(request, oldMeal.getMealImage());
			oldMeal.setMealImage(img);
		}
		oldMeal.setMealName(meal.getMealName());
		oldMeal.setMealPrice(meal.getMealPrice());
		oldMeal.setMealSummarize(meal.getMealSummarize());
		oldMeal.setMealSeries(mealSeriesService.getEntityById(seriesId));
		mealService.saveOrUpdate(oldMeal);
		return Constant.SUCCESS.toString();
	}
	// count meal page
		@RequestMapping("/countMealPage")
		@ResponseBody
		public String countMealPage(HttpServletRequest request) {
			return mealService.countPage();
		}
		// find meal by page
		@RequestMapping("/findMealPage")
		@ResponseBody
		public List<Meal> findMealPage(int page, HttpServletRequest request) {
			return mealService.findAllByCondition(null, null, null, page, Constant.PAGE_SIZE);
		}
		// get one page by id
		@RequestMapping("/getMeal")
		@ResponseBody
		public Meal getMeal(int id, HttpServletRequest request) {
			return mealService.getEntityById(id);
		}
		// init meal
		@RequestMapping("/initMeal")
		@ResponseBody
		public List<Meal> initMeal( HttpServletRequest request) {
			List<Meal> list = mealService.findAllByField("mealSeries.seriesName", "Famous cuisine");
			if(list.size() <= 11) {
				return list;
			}else{
				return list.subList(0, 11);
			}
			
		}
		// find meal by mealseries or keyword
		@RequestMapping("/findMealByCondition")
		@ResponseBody
		public List<Meal> findMealByCondition(Integer op,int page,@RequestParam(required=false,defaultValue="0") Integer seriesId,
				@RequestParam(required=false,defaultValue="") String keyword,
				HttpServletRequest request) {
			if(op.equals(Constant.PAGE_OP_ALL)) {
				return mealService.findAllByCondition(null, null, null, page, Constant.USER_PAGE_SIZE);
			}else if(op.equals(Constant.PAGE_OP_MS)) {
				return mealService.findAllByCondition("and mealSeries.id=?", new Object[]{seriesId}, null, page, Constant.USER_PAGE_SIZE);
			}else if(op.equals(Constant.PAGE_OP_KEYWORD)) {
				return mealService.findAllByCondition("and mealName like ?", new Object[]{"%"+keyword+"%"}, null, page, Constant.USER_PAGE_SIZE);
			}
			return null;
		}
		// count meal page by condition
		@RequestMapping("/countConditionMealPage")
		@ResponseBody
		public String countConditionMealPage(Integer op,@RequestParam(required=false,defaultValue="0") Integer seriesId,
				@RequestParam(required=false,defaultValue="") String keyword,
				HttpServletRequest request) {
			if(op.equals(Constant.PAGE_OP_ALL)) {
				return mealService.countPage();
			}else if(op.equals(Constant.PAGE_OP_MS)) {
				return calPage(mealService.findTotalRecordsNum("and mealSeries.id=?", new Object[]{seriesId}),Constant.USER_PAGE_SIZE);
			}else if(op.equals(Constant.PAGE_OP_KEYWORD)) {
				return calPage(mealService.findTotalRecordsNum("and mealName like ?", new Object[]{"%"+keyword+"%"}),Constant.USER_PAGE_SIZE);
			}
			return null;
		}
		// add to cart
		@RequestMapping("/addToCart")
		@ResponseBody
		public String addToCart(int mealId,int count,HttpServletRequest request) {
			Users user = (Users) request.getSession().getAttribute("user");
			if(user == null) {
				return Constant.USER_NOT_LOGIN.toString();
			}
			Meal meal = mealService.getEntityById(mealId);
			Cart cart = user.getCart();
			for(CartItem ci : cart.getCartItemSet()) {
				if(ci.getMeal().getId().equals(mealId)) {
					ci.setMealCount(ci.getMealCount()+count);
					cart.setCartPrice(cart.getCartPrice()+count *ci.getMealPrice() );
					cartService.saveOrUpdate(cart);
					return Constant.SUCCESS.toString();
				}
			}
			CartItem ci = new CartItem(null, meal, meal.getMealPrice(), count);
			cart.setCartPrice(cart.getCartPrice()+count *ci.getMealPrice() );
			cart.getCartItemSet().add(ci);
			cartService.saveOrUpdate(cart);
			return Constant.SUCCESS.toString();
		}
		
		// delete cart item
		@RequestMapping("/deleteCartItem")
		@ResponseBody
		public String deleteCartItem(int cartItemId,HttpServletRequest request) {
			Users user = (Users) request.getSession().getAttribute("user");
			if(user == null) {
				return Constant.USER_NOT_LOGIN.toString();
			}
			Cart cart = user.getCart();
			CartItem del = null;
			HashSet<CartItem> set = new HashSet<CartItem>();
			for(CartItem ci : cart.getCartItemSet()) {
				if(ci.getId().equals(cartItemId)) {
					del = ci;
					continue;
				}
				set.add(ci);
			}
			if(del != null) {
				cart.setCartPrice(cart.getCartPrice() - del.getMealCount()*del.getMealPrice());
				cart.setCartItemSet(set);
				cartService.saveOrUpdate(cart);
				cartItemService.deleteEntityByIds(del.getId());
			}
			return Constant.SUCCESS.toString();
		}
		// change cart item count
		@RequestMapping("/changeCount")
		@ResponseBody
		public String changeCartItemCount(int cartItemId, int count,HttpServletRequest request) {
			Users user = (Users) request.getSession().getAttribute("user");
			if(user == null) {
				return Constant.USER_NOT_LOGIN.toString();
			}
			Cart cart = user.getCart();
			CartItem change = null;
			for(CartItem ci : cart.getCartItemSet()) {
				if(ci.getId().equals(cartItemId)) {
					change = ci;
					break;
				}
			}
			if(change != null) {
				cart.setCartPrice(cart.getCartPrice() - change.getMealCount()*change.getMealPrice());
				change.setMealCount(count);
				cart.setCartPrice(cart.getCartPrice() + change.getMealCount()*change.getMealPrice());
				cartService.saveOrUpdate(cart);
			}
			return Constant.SUCCESS.toString();
		}
		// change cart item count
		@RequestMapping("/addOn")
		@ResponseBody
		public String addOn(String addOn,HttpServletRequest request) {
			Users user = (Users) request.getSession().getAttribute("user");
			if(user == null) {
				return Constant.USER_NOT_LOGIN.toString();
			}
			if(user.getCart().getCartItemSet().size() == 0) {
				return Constant.PAY_REPAET.toString();
			}
			user.getCart().setAddOn(addOn);
			return Constant.SUCCESS.toString();
		}
		// change cart item count
		@RequestMapping("/payOrder")
		@ResponseBody
		public String payOrder(HttpServletRequest request) {
			Users user = (Users) request.getSession().getAttribute("user");
			if(user == null) {
				return Constant.USER_NOT_LOGIN.toString();
			}
			mealService.addOrder(user);
			return Constant.SUCCESS.toString();
		}
		
		// get cart item count
		@RequestMapping("/getCartItem")
		@ResponseBody
		public List<CartItem> getCartItem(HttpServletRequest request) {
			Users user = (Users) request.getSession().getAttribute("user");
			if(user == null) {
				return null;
			}
			return new ArrayList<CartItem>(user.getCart().getCartItemSet());
		}
		// get cart item count
		@RequestMapping("/getCart")
		@ResponseBody
		public Cart getCart(HttpServletRequest request) {
			Users user = (Users) request.getSession().getAttribute("user");
			if(user == null) {
				return null;
			}
			return usersService.getEntityById(user.getId()).getCart();
		}
		private String calPage(int count,int page) {
			Integer size = 0;
			if(count % page == 0) {
				size = count /page;
			}else {
				size = count / page + 1;
			}
			return size.toString();
		}
	}

	
