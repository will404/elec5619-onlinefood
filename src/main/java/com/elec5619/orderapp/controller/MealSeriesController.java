package com.elec5619.orderapp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.elec5619.orderapp.constant.Constant;
import com.elec5619.orderapp.domain.MealSeries;
import com.elec5619.orderapp.service.MealSeriesService;

@Controller
@RequestMapping("/mealSeries")
public class MealSeriesController {

	@Autowired
	private MealSeriesService mealSeriesService;
	
	// add MealSeries
	@RequestMapping("/addMS")
	@ResponseBody
	public String addMealSeries(MealSeries ms, HttpServletRequest request) {
		System.out.println(ms);
		System.out.println(request.getParameter("seriesName"));
		if(mealSeriesService.findByMealSeriesName(ms.getSeriesName()) != null) {
			return Constant.MS_EXIST.toString();
		}
		mealSeriesService.saveOrUpdate(ms);
		return Constant.SUCCESS.toString();
	}
	
	// find all MealSeries by page
	@RequestMapping("/findMSPage")
	@ResponseBody
	public List<MealSeries> findMSPage(int page, HttpServletRequest request) {
		return mealSeriesService.findAllByCondition(null, null, null, page, Constant.PAGE_SIZE);
	}
	// find all MealSeries 
	@RequestMapping("/findMSAll")
	@ResponseBody
	public List<MealSeries> findMSAll(HttpServletRequest request) {
		return mealSeriesService.findAllEntity();
	}
	// count all page MealSeries
	@RequestMapping("/countMSPage")
	@ResponseBody
	public String countMSPage(HttpServletRequest request) {
	
		
		return mealSeriesService.countPage();
	}
	// update MealSeries
	@RequestMapping("/updateMS")
	@ResponseBody
	public String updateMealSeries(MealSeries ms, HttpServletRequest request) {
		if(mealSeriesService.findByMealSeriesName(ms.getSeriesName()) != null) {
			return Constant.MS_EXIST.toString();
		}
		mealSeriesService.saveOrUpdate(ms);
		return Constant.SUCCESS.toString();
	}
	// get one MealSeries by id
	@RequestMapping("/getMS")
	@ResponseBody
	public MealSeries getMealSeries(HttpServletRequest request, int id) {
		return mealSeriesService.getEntityById(id);
	}
 	
	// delete MealSeries
	@RequestMapping("/deleteMS")
	@ResponseBody
	public String delMealSeries(int id, HttpServletRequest request) {
		if(mealSeriesService.isMealSeriesEmpty(id).equals(Constant.MS_NOT_EMPTY.toString())) {
			return Constant.MS_NOT_EMPTY.toString();
		}
		mealSeriesService.deleteEntityByIds(id);
		return Constant.SUCCESS.toString();
	}
	
}