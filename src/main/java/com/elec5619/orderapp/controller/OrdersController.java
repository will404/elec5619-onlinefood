package com.elec5619.orderapp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.elec5619.orderapp.constant.Constant;
import com.elec5619.orderapp.domain.Orders;
import com.elec5619.orderapp.domain.OrdersItem;
import com.elec5619.orderapp.domain.Users;
import com.elec5619.orderapp.service.OrdersItemService;
import com.elec5619.orderapp.service.OrdersService;

@Controller
@RequestMapping("/orders")
public class OrdersController {
	@Autowired
	private OrdersService ordersService;
	
	@Autowired
	private OrdersItemService ordersItemService;
	// count Orders page
	@RequestMapping("/countOrdersPage")
	@ResponseBody
	public String countOrdersPage(HttpServletRequest request) {
		return ordersService.countPage();
	}
	// find Orders by page
	@RequestMapping("/findOrdersPage")
	@ResponseBody
	public List<Orders> findOrdersPage(int page, HttpServletRequest request) {
		return ordersService.findAllByCondition(null, null, null, page, Constant.PAGE_SIZE);
	}
	// get one page by id
	@RequestMapping("/getOrders")
	@ResponseBody
	public Orders getOrders(int id, HttpServletRequest request) {
		return ordersService.getEntityById(id);
	}
	// confirm Orders
	@RequestMapping("/confirmOrders")
	@ResponseBody
	public String confirmOrders(int id,HttpServletRequest request) {
		Orders orders = ordersService.getEntityById(id);
		if(orders.getOrderState().equals("Payed")) {
			orders.setOrderState("Deliver Goods");
		}
		ordersService.saveOrUpdate(orders);
		return Constant.SUCCESS.toString();
	}
	
	/********************user order method*************/
	// count Orders page
	@RequestMapping("/countUserOrdersPage")
	@ResponseBody
	public String countUserOrdersPage(HttpServletRequest request) {
		Users user = (Users) request.getSession().getAttribute("user");
		if(user == null) {
			return Constant.USER_NOT_LOGIN.toString();
		}
		return calPage(ordersService.findTotalRecordsNum("and user.id=?", new Object[]{user.getId()}),Constant.USER_PAGE_SIZE);
	}
	// get order item
	@RequestMapping("/getOrderItem")
	@ResponseBody
	public OrdersItem getOrderItem(int id,HttpServletRequest request) {
		Users user = (Users) request.getSession().getAttribute("user");
		if(user == null) {
			return null;
		}
		return ordersItemService.getEntityById(id);
	}
	// find Orders by page
	@RequestMapping("/findUserOrdersPage")
	@ResponseBody
	public List<Orders> findUserOrdersPage(int page, HttpServletRequest request) {
		Users user = (Users) request.getSession().getAttribute("user");
		if(user == null) {
			return null;
		}
		return ordersService.findAllByCondition("and user.id=?", new Object[]{user.getId()}, null, page, Constant.USER_PAGE_SIZE);
	}
	private String calPage(int count,int page) {
		Integer size = 0;
		if(count % page == 0) {
			size = count /page;
		}else {
			size = count / page + 1;
		}
		return size.toString();
	}
	
}
