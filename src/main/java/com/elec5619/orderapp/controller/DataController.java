package com.elec5619.orderapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.elec5619.orderapp.service.MealSeriesService;
import com.elec5619.orderapp.service.MealService;
import com.elec5619.orderapp.service.OrdersService;
import com.elec5619.orderapp.service.UsersService;

@Controller()
@RequestMapping("/data")
public class DataController {
	@Autowired
	protected MealService mealService;
	@Autowired
	protected OrdersService ordersService;
	@Autowired
	protected UsersService usersService;
	@Autowired
	protected MealSeriesService mealSeriesService;
	
	@RequestMapping("/countUser")
	@ResponseBody
	public String countUser() {
		return usersService.findTotalRecordsNum(null, null)+"";
	}
	@RequestMapping("/countMeal")
	@ResponseBody
	public String countMeal() {
		return mealService.findTotalRecordsNum(null, null)+"";
	}
	@RequestMapping("/countOrder")
	@ResponseBody
	public String countOrder() {
		return ordersService.findTotalRecordsNum(null, null)+"";
	}
	@RequestMapping("/countMealSeries")
	@ResponseBody
	public String countMealSeries() {
		return mealSeriesService.findTotalRecordsNum(null, null)+"";
	}
}
