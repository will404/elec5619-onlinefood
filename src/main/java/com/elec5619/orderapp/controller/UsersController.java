package com.elec5619.orderapp.controller;

import java.util.HashSet;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.elec5619.orderapp.constant.Constant;
import com.elec5619.orderapp.domain.Cart;
import com.elec5619.orderapp.domain.CartItem;
import com.elec5619.orderapp.domain.Users;
import com.elec5619.orderapp.service.CartService;
import com.elec5619.orderapp.service.UsersService;
import com.elec5619.orderapp.utils.MD5Utils;

@Controller
@RequestMapping("/user")
public class UsersController {
	@Autowired
	private UsersService usersService;
	@Autowired
	private CartService cartService;
	// login user
	@RequestMapping("/loginUsers")
	@ResponseBody
	public String loginUsers(HttpServletRequest request,Users users) {
		Users u = usersService.loginUsers(users);
		if(u == null) {
			return Constant.USER_PWD_WRONG.toString();
		}else {
			if(u.getCart().getCartItemSet() == null) {
				u.getCart().setCartItemSet(new HashSet<CartItem>());
			}
			request.getSession().setAttribute("user", u);
			return Constant.SUCCESS.toString();
		}
	}
	@RequestMapping("/getUser")
	@ResponseBody
	public Users getUser(HttpServletRequest request,Users users) {
		Users user = (Users) request.getSession().getAttribute("user");
		if(user == null) {
			return null;
		}
		return user;
	}
	// register user
	@RequestMapping("/updateUsers")
	@ResponseBody
	public String updateUsers(HttpServletRequest request,Users users) {
		Users user = (Users) request.getSession().getAttribute("user");
		if(user == null) {
			return Constant.USER_NOT_LOGIN.toString();
		}
		user.setLoginPwd(MD5Utils.md5(users.getLoginPwd()));
		user.setAddress(users.getAddress());
		user.setDisplayName(users.getDisplayName());
		user.setEmail(users.getEmail());
		user.setPhone(users.getPhone());
		usersService.saveOrUpdate(user);
		return Constant.SUCCESS.toString();
	}
	
	// register user
	@RequestMapping("/registerUsers")
	@ResponseBody
	public String registerUsers(HttpServletRequest request,Users users) {
		Cart cart = new Cart();
		cartService.saveOrUpdate(cart);
		users.setCart(cart);
		users.setLoginPwd(MD5Utils.md5(users.getLoginPwd()));
		usersService.saveOrUpdate(users);
		return Constant.SUCCESS.toString();
	}
	
	// check login name
	@RequestMapping("/checkLoginName")
	@ResponseBody
	public String checkLoginName(HttpServletRequest request,Users users) {
		return usersService.checkLoginName(users);
	}
	// exit user
	@RequestMapping("/logoutUsers")
	public String logoutUsers(HttpServletRequest request) {
		
		request.getSession().removeAttribute("user");
		return "redirect:/page/index.jsp";
	}
	
	// login Admin
	@RequestMapping("/adminLogin")
	@ResponseBody
	public String adminLogin(HttpServletRequest request,String username,String password) {
		if(!username.equals("admin")) {
			return "2";
		}
		if(!MD5Utils.md5(password).equals(Constant.getAdminPassword())) {
			return "3";
		}
		request.getSession().setAttribute("Admin", username);
		return "1";
	}
	
	@RequestMapping("/adminExit")
	public String adminExit(HttpServletRequest request) {
		request.getSession().removeAttribute("Admin");
		return "redirect:/login.jsp";
	}
}
