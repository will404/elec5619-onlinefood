package com.elec5619.orderapp.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
public class AdminFilter implements Filter{

	
	public void destroy() {
		
	}

	
	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) arg0;
		if(request.getRequestURI().contains("admin") && request.getRequestURI().contains("jsp")) {
			if(request.getSession().getAttribute("Admin")!=null) {
				arg2.doFilter(arg0, arg1);
			}else{
				HttpServletResponse r = (HttpServletResponse) arg1;
				r.sendRedirect(request.getContextPath()+"/login.jsp");
			}
		}else{
			arg2.doFilter(arg0, arg1);
		}
		
	}

	
	public void init(FilterConfig arg0) throws ServletException {
		
	}

}
