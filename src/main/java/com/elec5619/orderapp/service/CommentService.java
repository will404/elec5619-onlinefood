package com.elec5619.orderapp.service;

import com.elec5619.orderapp.base.service.BaseService;
import com.elec5619.orderapp.domain.Comment;

public interface CommentService extends BaseService<Comment>{

}
