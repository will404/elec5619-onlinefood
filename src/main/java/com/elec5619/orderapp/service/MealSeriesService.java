package com.elec5619.orderapp.service;

import com.elec5619.orderapp.base.service.BaseService;
import com.elec5619.orderapp.domain.MealSeries;

public interface MealSeriesService extends BaseService<MealSeries>{
	MealSeries findByMealSeriesName(String name);
	String isMealSeriesEmpty(Integer id);
}
