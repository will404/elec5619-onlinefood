package com.elec5619.orderapp.service;

import com.elec5619.orderapp.base.service.BaseService;
import com.elec5619.orderapp.domain.CartItem;

public interface CartItemService extends BaseService<CartItem>{
	
}
