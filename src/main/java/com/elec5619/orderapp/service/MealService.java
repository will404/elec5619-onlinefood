package com.elec5619.orderapp.service;

import com.elec5619.orderapp.base.service.BaseService;
import com.elec5619.orderapp.domain.Meal;
import com.elec5619.orderapp.domain.MealSeries;
import com.elec5619.orderapp.domain.Users;

public interface MealService extends BaseService<Meal>{
	
	Meal findByMealByName(String name);
	String hasOrder(int id);
	void addOrder(Users user);
}
