package com.elec5619.orderapp.service.impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.elec5619.orderapp.base.dao.BaseDao;
import com.elec5619.orderapp.base.service.impl.BaseServiceImpl;
import com.elec5619.orderapp.dao.CartDao;
import com.elec5619.orderapp.domain.Cart;
import com.elec5619.orderapp.service.CartService;

@Service
@Transactional
public class CartServiceImpl extends BaseServiceImpl<Cart> implements CartService{
	
	@Autowired
	private CartDao cartDao;
	
	@Override
	public BaseDao<Cart> getBaseDao() {
		// TODO Auto-generated method stub
		return cartDao;
	}

}
