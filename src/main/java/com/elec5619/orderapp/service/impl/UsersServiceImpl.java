package com.elec5619.orderapp.service.impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.elec5619.orderapp.base.dao.BaseDao;
import com.elec5619.orderapp.base.service.impl.BaseServiceImpl;
import com.elec5619.orderapp.constant.Constant;
import com.elec5619.orderapp.dao.UsersDao;
import com.elec5619.orderapp.domain.Users;
import com.elec5619.orderapp.service.UsersService;
import com.elec5619.orderapp.utils.MD5Utils;

@Service
@Transactional
public class UsersServiceImpl extends BaseServiceImpl<Users> implements UsersService{
	
	@Autowired
	private UsersDao usersDao;
	
	@Override
	public BaseDao<Users> getBaseDao() {
		return usersDao;
	}


	public String checkLoginName(Users users) {
		if(usersDao.findOneByField("loginName", users.getLoginName()) == null) {
			return Constant.SUCCESS.toString();
		}
		return Constant.USER_EXIST.toString();
	}


	public Users loginUsers(Users users) {
		return usersDao.getOneByCondition("and loginName=? and loginPwd=?", new Object[]{users.getLoginName(),MD5Utils.md5(users.getLoginPwd())});
	}

}
