package com.elec5619.orderapp.service.impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.elec5619.orderapp.base.dao.BaseDao;
import com.elec5619.orderapp.base.service.impl.BaseServiceImpl;
import com.elec5619.orderapp.dao.CartItemDao;
import com.elec5619.orderapp.domain.CartItem;
import com.elec5619.orderapp.service.CartItemService;

@Service
@Transactional
public class CartItemServiceImpl extends BaseServiceImpl<CartItem> implements CartItemService{
	
	@Autowired
	private CartItemDao cartItemDao;
	
	@Override
	public BaseDao<CartItem> getBaseDao() {
		// TODO Auto-generated method stub
		return cartItemDao;
	}

}
