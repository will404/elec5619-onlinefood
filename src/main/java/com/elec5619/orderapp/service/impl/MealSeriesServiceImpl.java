package com.elec5619.orderapp.service.impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.elec5619.orderapp.base.dao.BaseDao;
import com.elec5619.orderapp.base.service.impl.BaseServiceImpl;
import com.elec5619.orderapp.constant.Constant;
import com.elec5619.orderapp.dao.MealSeriesDao;
import com.elec5619.orderapp.domain.MealSeries;
import com.elec5619.orderapp.service.MealSeriesService;
import com.elec5619.orderapp.service.MealService;

@Service
@Transactional
public class MealSeriesServiceImpl extends BaseServiceImpl<MealSeries> implements MealSeriesService{
	
	@Autowired
	private MealSeriesDao mealSeriesDao;
	
	@Autowired
	private MealService mealService;
	
	@Override
	public BaseDao<MealSeries> getBaseDao() {
		// TODO Auto-generated method stub
		return mealSeriesDao;
	}

	
	public MealSeries findByMealSeriesName(String name) {
		return mealSeriesDao.findOneByField("seriesName", name);
	}

	
	public String isMealSeriesEmpty(Integer id) {
		int size = mealService.findAllByCondition("and mealSeries.id=?", new Object[]{id}, null).size();
		if(size == 0) {
			return Constant.MS_EMPTY.toString();
		}
		return Constant.MS_NOT_EMPTY.toString();
	}

}
