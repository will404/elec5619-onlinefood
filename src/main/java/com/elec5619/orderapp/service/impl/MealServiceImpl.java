package com.elec5619.orderapp.service.impl;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.elec5619.orderapp.base.dao.BaseDao;
import com.elec5619.orderapp.base.service.impl.BaseServiceImpl;
import com.elec5619.orderapp.constant.Constant;
import com.elec5619.orderapp.dao.MealDao;
import com.elec5619.orderapp.domain.Cart;
import com.elec5619.orderapp.domain.CartItem;
import com.elec5619.orderapp.domain.Meal;
import com.elec5619.orderapp.domain.Orders;
import com.elec5619.orderapp.domain.OrdersItem;
import com.elec5619.orderapp.domain.Users;
import com.elec5619.orderapp.service.CartItemService;
import com.elec5619.orderapp.service.CartService;
import com.elec5619.orderapp.service.MealService;
import com.elec5619.orderapp.service.OrdersItemService;
import com.elec5619.orderapp.service.OrdersService;

@Service
@Transactional
public class MealServiceImpl extends BaseServiceImpl<Meal> implements MealService{
	
	@Autowired
	private MealDao mealDao;
	@Autowired
	private OrdersItemService ordersItemService;
	@Autowired
	protected OrdersService ordersService;
	@Autowired
	protected CartService cartService;
	@Autowired
	protected CartItemService cartItemService;
	@Override
	public BaseDao<Meal> getBaseDao() {
		return mealDao;
	}

	@Override
	public void saveOrUpdate(Meal entity) {
		super.saveOrUpdate(entity);
	}

	public Meal findByMealByName(String name) {
		return mealDao.findOneByField("mealName", name);
	}

	
	public String hasOrder(int id) {
		int size = ordersItemService.findAllByCondition("and meal.id=?", new Object[] {id}, null).size();
		if(size == 0) {
			return Constant.MEAL_NOT_ORDER.toString();
		}
		return Constant.MEAL_HAS_ORDER.toString();
	}

	
	public void addOrder(Users user) {
		Cart cart = user.getCart();
		Orders order = new Orders(null, user, new Date(), Constant.ORDER_STATE, cart.getCartPrice(), cart.getAddOn());
		// add orders item
		HashSet<OrdersItem> set = new HashSet<OrdersItem>();
    	for(CartItem c:cart.getCartItemSet()) {
    		OrdersItem item = new OrdersItem(null, c.getMeal(), c.getMealPrice(), c.getMealCount());
    		set.add(item);
    	}
		order.setOrdersItemSet(set);
		ordersService.saveOrUpdate(order);
		// delete cart
		Set<CartItem> ciSet = cart.getCartItemSet();
    	cart.setCartItemSet(new HashSet<CartItem>());
    	cart.setAddOn("");
    	cart.setCartPrice(0);
    	cartService.saveOrUpdate(cart);
    	cartItemService.deleteEntityByCollection(new ArrayList<CartItem>(ciSet));
	}
	
	

}
