package com.elec5619.orderapp.service.impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.elec5619.orderapp.base.dao.BaseDao;
import com.elec5619.orderapp.base.service.impl.BaseServiceImpl;
import com.elec5619.orderapp.dao.OrdersDao;
import com.elec5619.orderapp.domain.Orders;
import com.elec5619.orderapp.service.OrdersService;

@Service
@Transactional
public class OrdersServiceImpl extends BaseServiceImpl<Orders> implements OrdersService{
	
	@Autowired
	private OrdersDao ordersDao;
	
	@Override
	public BaseDao<Orders> getBaseDao() {
		// TODO Auto-generated method stub
		return ordersDao;
	}

}
