package com.elec5619.orderapp.service.impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.elec5619.orderapp.base.dao.BaseDao;
import com.elec5619.orderapp.base.service.impl.BaseServiceImpl;
import com.elec5619.orderapp.dao.OrdersItemDao;
import com.elec5619.orderapp.domain.OrdersItem;
import com.elec5619.orderapp.service.OrdersItemService;

@Service
@Transactional
public class OrdersItemServiceImpl extends BaseServiceImpl<OrdersItem> implements OrdersItemService{
	
	@Autowired
	private OrdersItemDao ordersItemDao;
	
	@Override
	public BaseDao<OrdersItem> getBaseDao() {
		// TODO Auto-generated method stub
		return ordersItemDao;
	}

}
