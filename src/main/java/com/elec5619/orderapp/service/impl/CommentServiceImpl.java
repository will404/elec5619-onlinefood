package com.elec5619.orderapp.service.impl;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.elec5619.orderapp.base.dao.BaseDao;
import com.elec5619.orderapp.base.service.impl.BaseServiceImpl;
import com.elec5619.orderapp.dao.CommentDao;
import com.elec5619.orderapp.domain.Comment;
import com.elec5619.orderapp.domain.Meal;
import com.elec5619.orderapp.service.CommentService;
import com.elec5619.orderapp.service.MealService;

@Service
@Transactional
public class CommentServiceImpl extends BaseServiceImpl<Comment> implements CommentService{
	
	@Autowired
	private CommentDao commentDao;
	@Autowired
	private MealService mealService;
	@Override
	public BaseDao<Comment> getBaseDao() {
		// TODO Auto-generated method stub
		return commentDao;
	}
	
	@Override
	public void saveOrUpdate(Comment entity) {
		Meal meal = mealService.getEntityById(entity.getMeal().getId());
		List<Comment> list = commentDao.findAllByField("meal.id", meal.getId());
		int count = entity.getRating();
		for(Comment i : list) {
			count+=i.getRating();
		}
		meal.setRating(count/(list.size()+1));
		mealService.saveOrUpdate(meal);
		super.saveOrUpdate(entity);
	}
}
