package com.elec5619.orderapp.service;

import com.elec5619.orderapp.base.service.BaseService;
import com.elec5619.orderapp.domain.Users;

public interface UsersService extends BaseService<Users>{
	String checkLoginName(Users users);
	Users loginUsers(Users users);
}
