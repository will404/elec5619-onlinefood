package com.elec5619.orderapp.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.junit.Test;

import com.elec5619.orderapp.domain.Cart;
import com.elec5619.orderapp.domain.CartItem;
import com.elec5619.orderapp.domain.Comment;
import com.elec5619.orderapp.domain.Meal;
import com.elec5619.orderapp.domain.Orders;
import com.elec5619.orderapp.domain.OrdersItem;
import com.elec5619.orderapp.domain.Users;

public class HelloTest extends BaseTest {

    @Test
    public void getTicketInfo() {
    	for(int i = 0;i<10;i++) {
    		
    		usersService.saveOrUpdate(new Users(null, "test"+i, "test"+i, "test"+i, "test@qq.com", "2342434", "fsdfsfsf"));
    	}
    }
    @Test
    public void addOrder() {
    	Users users = usersService.getEntityById(2);
    	List<Meal> list = mealService.findAllEntity();
    	for(int i = 0;i<25;i++) {
    		Orders o = new Orders(null,users,new Date(),"unpay",15,"null");
    		Set<OrdersItem> set = new HashSet<OrdersItem>();
    		set.add(new OrdersItem(null, list.get(0), list.get(0).getMealPrice(), 5));
    		o.setOrdersItemSet(set);
    		ordersService.saveOrUpdate(o);
    	}
    }
    
    @Test
    public void addOrderi() {
    	Users users = usersService.getEntityById(2);
    	List<Meal> list = mealService.findAllEntity();
    	for(int i = 0;i<25;i++) {
    		Orders o = new Orders(null,users,new Date(),"unpay",15,"null");
    		ordersService.saveOrUpdate(o);
    	}
    }
    
    @Test
    public void addC() {
    	Users users = usersService.getEntityById(2);
    	List<Meal> list = mealService.findAllEntity();
    	for(int i = 0;i<15;i++) {
    		Random ran = new Random();
    		commentService.saveOrUpdate(new Comment(null, list.get(0), users, ran.nextInt(5) + 1, "dsfdsfsdafsdfwfewfwfdsfsdafsdfwfewfwfdsfsdafsdfwfewfwfdsfsdafsdfwfewfwfsdafsdfwfewfwf"+i));
    	}
    }
    @Test
    public void addCart() {
    	Users users = usersService.getEntityById(14);
    	
    	List<Meal> list = mealService.findAllEntity();
    	HashSet<CartItem> set = new HashSet<CartItem>();
    	int sum = 0;
    	for(int i = 0;i<5;i++) {
    		Random ran = new Random();
    		Integer price = list.get(0).getMealPrice();
    		int count = ran.nextInt(10);
    		CartItem item = new CartItem(null, list.get(0), price, count);
    		sum+=price*count;
    		set.add(item);
    	}
    	
    	users.getCart().setCartItemSet(set);
    	users.getCart().setCartPrice(sum);
    	cartService.saveOrUpdate(users.getCart());
    }
    
    @Test
    public void del() {
    	Users users = usersService.getEntityById(2);
    	Cart cart = users.getCart();
    	CartItem del = null;
		for(CartItem ci : cart.getCartItemSet()) {
			if(ci.getId().equals(7)) {
				del = ci;
				break;
			}
		}
		if(del != null) {
			del.setMealCount(100);
		}
    	cartService.saveOrUpdate(users.getCart());
    }
    @Test
    public void delall() {
    	Users users = usersService.getEntityById(2);
    	Cart cart = users.getCart();
    	Set<CartItem> set = cart.getCartItemSet();
    	cart.setCartItemSet(null);
    	cartService.saveOrUpdate(users.getCart());
    	cartItemService.deleteEntityByCollection(new ArrayList<CartItem>(set));
    }
    
    @Test
    public void apy() {
    	Users users = usersService.getEntityById(2);
    	mealService.addOrder(users);
    }
}