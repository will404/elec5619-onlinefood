package com.elec5619.orderapp.test;


import com.elec5619.orderapp.domain.MealSeries;

import junit.framework.TestCase;

public class MealSeriesTest extends TestCase{
	
	    private MealSeries mealSeries;
		
		protected void setUp() throws Exception{
			mealSeries = new MealSeries();
		}
		
		public void testSetGetName(){
			String mealSeriesName = "Drink";
			assertNull(mealSeries.getSeriesName());
			mealSeries.setSeriesName(mealSeriesName);
			assertEquals(mealSeriesName, mealSeries.getSeriesName());
		}
		

}
