package com.elec5619.orderapp.test;

import com.elec5619.orderapp.domain.Meal;

import junit.framework.TestCase;

public class MealTest extends TestCase {
	
	private Meal meal;
	
	protected void setUp() throws Exception{
		meal = new Meal();
	}
	
	public void testSetGetName(){
		String mealName = "Soup";
		assertNull(meal.getMealName());
		meal.setMealName(mealName);
		assertEquals(mealName, meal.getMealName());
	}
	
	public void testSetGetPrice(){
		Integer mealPrice = 12;
		assertNull(meal.getMealPrice());
		meal.setMealPrice(mealPrice);
		assertEquals(mealPrice, meal.getMealPrice());
	}
	
	public void testSetGetDes(){
		String mealDescription = "anything";
		assertNull(meal.getMealDescription());
		meal.setMealDescription(mealDescription);
		assertEquals(mealDescription, meal.getMealDescription());
	}
}
