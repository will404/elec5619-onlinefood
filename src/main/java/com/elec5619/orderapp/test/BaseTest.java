package com.elec5619.orderapp.test;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.elec5619.orderapp.service.CartItemService;
import com.elec5619.orderapp.service.CartService;
import com.elec5619.orderapp.service.CommentService;
import com.elec5619.orderapp.service.MealSeriesService;
import com.elec5619.orderapp.service.MealService;
import com.elec5619.orderapp.service.OrdersItemService;
import com.elec5619.orderapp.service.OrdersService;
import com.elec5619.orderapp.service.UsersService;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring.xml"})
public class BaseTest {
	@Autowired
	protected MealService mealService;
	@Autowired
	protected OrdersService ordersService;
	@Autowired
	protected UsersService usersService;
	@Autowired
	protected MealSeriesService mealSeriesService;
	@Autowired
	protected OrdersItemService ordersItemService;
	@Autowired
	protected CommentService commentService;
	@Autowired
	protected CartService cartService;
	@Autowired
	protected CartItemService cartItemService;
    @Before
    public void init() {
    	
    }
    @After
    public void after() {
    	
    }
}