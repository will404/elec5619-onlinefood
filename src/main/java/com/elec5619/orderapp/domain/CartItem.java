package com.elec5619.orderapp.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table
public class CartItem implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id()
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(nullable = false)
	private Integer id;
	@ManyToOne(targetEntity=Meal.class)
	private Meal meal;
	
	@Column(nullable = false)
	private Integer mealPrice;
	
	@Column(nullable = false)
	private Integer mealCount;

	public CartItem(Integer id, Meal meal, Integer mealPrice, Integer mealCount) {
		this.id = id;
		this.meal = meal;
		this.mealPrice = mealPrice;
		this.mealCount = mealCount;
	}

	public CartItem() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Meal getMeal() {
		return meal;
	}

	public void setMeal(Meal meal) {
		this.meal = meal;
	}
	public Double getMealPriceDouble() {
		return mealPrice/100.0;
	}
	public Integer getMealPrice() {
		return mealPrice;
	}

	public void setMealPrice(Integer mealPrice) {
		this.mealPrice = mealPrice;
	}

	public Integer getMealCount() {
		return mealCount;
	}

	public void setMealCount(Integer mealCount) {
		this.mealCount = mealCount;
	}

	@Override
	public String toString() {
		return "CartItem [id=" + id + ", meal=" + meal + ", mealPrice=" + mealPrice + ", mealCount=" + mealCount + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((meal == null) ? 0 : meal.hashCode());
		result = prime * result + ((mealCount == null) ? 0 : mealCount.hashCode());
		result = prime * result + ((mealPrice == null) ? 0 : mealPrice.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CartItem other = (CartItem) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (meal == null) {
			if (other.meal != null)
				return false;
		} else if (!meal.equals(other.meal))
			return false;
		if (mealCount == null) {
			if (other.mealCount != null)
				return false;
		} else if (!mealCount.equals(other.mealCount))
			return false;
		if (mealPrice == null) {
			if (other.mealPrice != null)
				return false;
		} else if (!mealPrice.equals(other.mealPrice))
			return false;
		return true;
	}
	
	
	
}
