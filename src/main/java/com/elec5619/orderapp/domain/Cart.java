package com.elec5619.orderapp.domain;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table
public class Cart implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id()
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(nullable = false)
	private Integer id;
	@Column(nullable = false)
	private Integer cartPrice;
	@Column(nullable = true)
	private String addOn;
	@OneToMany(targetEntity=CartItem.class,cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<CartItem> cartItemSet;
	public Cart(Integer id,Integer cartPrice, String addOn) {
		super();
		this.id = id;
		this.cartPrice = cartPrice;
		this.addOn = addOn;
	}
	public Cart() {
		cartPrice = 0;
		addOn = "";
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Double getCartPriceDouble() {
		return cartPrice/100.0;
	}
	public Integer getCartPrice() {
		return cartPrice;
	}
	public void setCartPrice(Integer cartPrice) {
		this.cartPrice = cartPrice;
	}
	public String getAddOn() {
		return addOn;
	}
	public void setAddOn(String addOn) {
		this.addOn = addOn;
	}
	public Set<CartItem> getCartItemSet() {
		return cartItemSet;
	}
	public void setCartItemSet(Set<CartItem> cartItemSet) {
		this.cartItemSet = cartItemSet;
	}
	@Override
	public String toString() {
		return "Cart [id=" + id +  ", cartPrice=" + cartPrice + ", addOn=" + addOn + ", cartItemSet="
				+ cartItemSet + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((addOn == null) ? 0 : addOn.hashCode());
		result = prime * result + ((cartItemSet == null) ? 0 : cartItemSet.hashCode());
		result = prime * result + ((cartPrice == null) ? 0 : cartPrice.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cart other = (Cart) obj;
		if (addOn == null) {
			if (other.addOn != null)
				return false;
		} else if (!addOn.equals(other.addOn))
			return false;
		if (cartItemSet == null) {
			if (other.cartItemSet != null)
				return false;
		} else if (!cartItemSet.equals(other.cartItemSet))
			return false;
		if (cartPrice == null) {
			if (other.cartPrice != null)
				return false;
		} else if (!cartPrice.equals(other.cartPrice))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
