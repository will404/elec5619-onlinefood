package com.elec5619.orderapp.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table
public class Meal implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id()
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(nullable = false)
	private Integer id;
	@Column(nullable = false)
	private String mealName;
	@Column(nullable = false)
	private String mealDescription;
	@Column(nullable = false)
	private Integer mealPrice;
	@Column(nullable = false)
	private String mealImage;
	@ManyToOne(targetEntity=MealSeries.class)
	private MealSeries mealSeries;
	@Column(nullable = false)
	private String mealSummarize;
	@Column(nullable = false)
	private Integer rating;
	public Meal() {
		rating = 0;
	}
	public Meal(Integer id, String mealName, String mealDescription, Integer mealPrice, String mealImage,
			MealSeries mealSeries, String mealSummarize) {
		this.id = id;
		this.mealName = mealName;
		this.mealDescription = mealDescription;
		this.mealPrice = mealPrice;
		this.mealImage = mealImage;
		this.mealSeries = mealSeries;
		this.mealSummarize = mealSummarize;
		rating = 0;
	}
	
	
	public Integer getRating() {
		return rating;
	}
	public void setRating(Integer rating) {
		this.rating = rating;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMealName() {
		return mealName;
	}
	public void setMealName(String mealName) {
		this.mealName = mealName;
	}
	public String getMealDescription() {
		return mealDescription;
	}
	public void setMealDescription(String mealDescription) {
		this.mealDescription = mealDescription;
	}
	public Double getMealPriceDouble() {
		return mealPrice/100.0;
	}
	public Integer getMealPrice() {
		return mealPrice;
	}
	public void setMealPrice(Integer mealPrice) {
		this.mealPrice = mealPrice;
	}
	public String getMealImage() {
		return mealImage;
	}
	public void setMealImage(String mealImage) {
		this.mealImage = mealImage;
	}
	public MealSeries getMealSeries() {
		return mealSeries;
	}
	public void setMealSeries(MealSeries mealSeries) {
		this.mealSeries = mealSeries;
	}
	public String getMealSummarize() {
		return mealSummarize;
	}
	public void setMealSummarize(String mealSummarize) {
		this.mealSummarize = mealSummarize;
	}
	@Override
	public String toString() {
		return "Meal [id=" + id + ", mealName=" + mealName + ", mealDescription=" + mealDescription + ", mealPrice="
				+ mealPrice + ", mealImage=" + mealImage + ", mealSeries=" + mealSeries + ", mealSummarize="
				+ mealSummarize + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((mealDescription == null) ? 0 : mealDescription.hashCode());
		result = prime * result + ((mealImage == null) ? 0 : mealImage.hashCode());
		result = prime * result + ((mealName == null) ? 0 : mealName.hashCode());
		result = prime * result + ((mealPrice == null) ? 0 : mealPrice.hashCode());
		result = prime * result + ((mealSeries == null) ? 0 : mealSeries.hashCode());
		result = prime * result + ((mealSummarize == null) ? 0 : mealSummarize.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Meal other = (Meal) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (mealDescription == null) {
			if (other.mealDescription != null)
				return false;
		} else if (!mealDescription.equals(other.mealDescription))
			return false;
		if (mealImage == null) {
			if (other.mealImage != null)
				return false;
		} else if (!mealImage.equals(other.mealImage))
			return false;
		if (mealName == null) {
			if (other.mealName != null)
				return false;
		} else if (!mealName.equals(other.mealName))
			return false;
		if (mealPrice == null) {
			if (other.mealPrice != null)
				return false;
		} else if (!mealPrice.equals(other.mealPrice))
			return false;
		if (mealSeries == null) {
			if (other.mealSeries != null)
				return false;
		} else if (!mealSeries.equals(other.mealSeries))
			return false;
		if (mealSummarize == null) {
			if (other.mealSummarize != null)
				return false;
		} else if (!mealSummarize.equals(other.mealSummarize))
			return false;
		return true;
	}
	
	
}
