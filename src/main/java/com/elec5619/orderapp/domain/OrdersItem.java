package com.elec5619.orderapp.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table
public class OrdersItem implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id()
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(nullable = false)
	private Integer id;
	@ManyToOne(targetEntity=Meal.class)
	private Meal meal;
	
	@Column(nullable = false)
	private Integer mealPrice;
	
	@Column(nullable = false)
	private Integer mealCount;
	@Column(nullable=false)
	private Integer commentState;
	
	public OrdersItem() {
		commentState = 0;
	}
	public OrdersItem(Integer id, Meal meal, Integer mealPrice, Integer mealCount) {
		this.id = id;
		this.meal = meal;
		this.mealPrice = mealPrice;
		this.mealCount = mealCount;
		commentState = 0;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Meal getMeal() {
		return meal;
	}
	public void setMeal(Meal meal) {
		this.meal = meal;
	}
	public Double getMealPriceDouble() {
		return mealPrice/100.0;
	}
	public Integer getMealPrice() {
		return mealPrice;
	}
	public void setMealPrice(Integer mealPrice) {
		this.mealPrice = mealPrice;
	}
	public Integer getMealCount() {
		return mealCount;
	}
	public void setMealCount(Integer mealCount) {
		this.mealCount = mealCount;
	}
	public String getCommentStateStr() {
		if(commentState.equals(0)) {
			return "UnComment";
		}else{
			return "Comment";
		}
	}
	public Integer getCommentState() {
		return commentState;
	}
	public void setCommentState(Integer commentState) {
		this.commentState = commentState;
	}
	@Override
	public String toString() {
		return "OrdersItem [id=" + id + ", meal=" + meal + ", mealPrice=" + mealPrice + ", mealCount=" + mealCount
				+ ", commentState=" + commentState + "]";
	}
	
}
