package com.elec5619.orderapp.domain;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table
public class Orders implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id()
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(nullable = false)
	private Integer id;
	
	@ManyToOne(targetEntity=Users.class)
	private Users user;
	@Column(nullable = false)
	private Date orderTime;
	@Column(nullable = false)
	private String orderState;
	@Column(nullable = false)
	private Integer orderPrice;
	@Column(nullable = false)
	private String addOn;
	@OneToMany(targetEntity=OrdersItem.class,cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<OrdersItem> ordersItemSet;
	
	public Orders() {
	}

	public Orders(Integer id, Users user, Date orderTime, String orderState, Integer orderPrice, String addOn) {
		this.id = id;
		this.user = user;
		this.orderTime = orderTime;
		this.orderState = orderState;
		this.orderPrice = orderPrice;
		this.addOn = addOn;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}
	
	public String getOrderTimeStr() {
		SimpleDateFormat format = new SimpleDateFormat("HH:mm dd/MM/yyyy");
		return format.format(orderTime);
	}
	public Date getOrderTime() {
		return orderTime;
	}

	public void setOrderTime(Date orderTime) {
		this.orderTime = orderTime;
	}

	public String getOrderState() {
		return orderState;
	}

	public void setOrderState(String orderState) {
		this.orderState = orderState;
	}

	
	public Set<OrdersItem> getOrdersItemSet() {
		return ordersItemSet;
	}

	public void setOrdersItemSet(Set<OrdersItem> ordersItemSet) {
		this.ordersItemSet = ordersItemSet;
	}

	public double getOrderPriceDouble() {
		
		return orderPrice/100.0;
	}
	public Integer getOrderPrice() {
		return orderPrice;
	}
	
	public void setOrderPrice(Integer orderPrice) {
		this.orderPrice = orderPrice;
	}

	public String getAddOn() {
		return addOn;
	}

	public void setAddOn(String addOn) {
		this.addOn = addOn;
	}

	@Override
	public String toString() {
		return "Orders [id=" + id + ", user=" + user + ", orderTime=" + orderTime + ", orderState=" + orderState
				+ ", orderPrice=" + orderPrice + ", addOn=" + addOn + ", ordersItemSet=" + ordersItemSet + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((addOn == null) ? 0 : addOn.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((orderPrice == null) ? 0 : orderPrice.hashCode());
		result = prime * result + ((orderState == null) ? 0 : orderState.hashCode());
		result = prime * result + ((orderTime == null) ? 0 : orderTime.hashCode());
		result = prime * result + ((ordersItemSet == null) ? 0 : ordersItemSet.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Orders other = (Orders) obj;
		if (addOn == null) {
			if (other.addOn != null)
				return false;
		} else if (!addOn.equals(other.addOn))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (orderPrice == null) {
			if (other.orderPrice != null)
				return false;
		} else if (!orderPrice.equals(other.orderPrice))
			return false;
		if (orderState == null) {
			if (other.orderState != null)
				return false;
		} else if (!orderState.equals(other.orderState))
			return false;
		if (orderTime == null) {
			if (other.orderTime != null)
				return false;
		} else if (!orderTime.equals(other.orderTime))
			return false;
		if (ordersItemSet == null) {
			if (other.ordersItemSet != null)
				return false;
		} else if (!ordersItemSet.equals(other.ordersItemSet))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	
	
}
