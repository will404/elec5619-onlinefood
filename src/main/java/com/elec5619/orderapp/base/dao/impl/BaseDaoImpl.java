package com.elec5619.orderapp.base.dao.impl;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.elec5619.orderapp.base.dao.BaseDao;
@SuppressWarnings("unchecked")
@Repository
public abstract class BaseDaoImpl<T> implements BaseDao<T>{

	@Autowired
	private SessionFactory sessionFactory;
	
	public BaseDaoImpl(){
	}
	
	protected abstract Class<?> getEntityType();
	
	
	@Transactional
	public void saveOrUpdate(T entity) {
		sessionFactory.getCurrentSession().saveOrUpdate(entity);
	}

	
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}


	
	
	public T getEntityById(Serializable id) {
		return (T) sessionFactory.getCurrentSession().get(getEntityType(), id);
	}

	
	public void deleteEntityByIds(Serializable... ids) {
		for(Serializable id : ids){
			Object object = sessionFactory.getCurrentSession().get(getEntityType(), id);
			sessionFactory.getCurrentSession().delete(object);
		}
	}

	
	public void deleteEntityByCollection(List<T> list) {
		for(Object object : list){
			sessionFactory.getCurrentSession().delete(object);
		}
	}

	public List<T> findAllEntity() {
		Query q = sessionFactory.getCurrentSession().createQuery("from " + getEntityType().getName());
		return (List<T>)q.list();
	}

	
	private String orderbyHql(Map<String, String> orderby){
		StringBuffer buffer = new StringBuffer("");
		
		if(orderby != null && orderby.size() > 0){
			buffer.append(" order by ");
			for(Map.Entry<String, String> en : orderby.entrySet()){
				buffer.append(en.getKey() + " " + en.getValue() + ",");
			}
			buffer.deleteCharAt(buffer.length() - 1);
		}else{
			return "";
		}
		
		return buffer.toString();
	}
	
	
	protected Query getQueryByCondition(String condition, Object[] params, Map<String, String> orderby){
		if(condition == null){
			condition = "";
		}
		
		String hql = "from " + getEntityType().getName() + " o where 1=1 ";
		String finalHql = hql + " " +condition + " " +orderbyHql(orderby);
		
		System.out.println("getQueryByCondition->finalHql->" + finalHql);
		Query query = sessionFactory.getCurrentSession().createQuery(finalHql);
		for(int i = 0; condition != null && params != null && i < params.length; i++){
			query.setParameter(i, params[i]);
		}
		return query;
	}
	
	
	
	public List<T> findAllByCondition(String condition, Object[] params, Map<String, String> orderby) {
		Query query = getQueryByCondition(condition, params, orderby);
		System.out.printf("getSessionFactory().getStatistics().getSecondLevelCachePutCount()->" + 
				getSessionFactory().getStatistics().getQueries());
		return query.list();
	}


	public int findTotalRecordsNum(String condition, Object[] params) {
		return findAllByCondition(condition, params, null).size();
	}

	
	public List<T> findAllByField(String field, Object value) {
		String sql = "from " + getEntityType().getName() + " where " + field + " = ? ";
		return (List<T>)sessionFactory.getCurrentSession().createQuery(sql).setParameter(0, value).list();
	}

	
	public T findOneByField(String field, Object value) {
		List<T> list = findAllByField(field, value);
		return (list == null || list.size() < 1) ? null : list.get(0);  
	}

	
	public List<T> findAllByCondition(String condition, Object[] params, Map<String, String> orderby, Integer page,
			Integer size) {
		Query query = getQueryByCondition(condition, params, orderby);
		
		if(page != null && size!= null){
			query.setMaxResults(size);
			query.setFirstResult((page-1) * size);
		}
		
		return query.list();
	}

	
	public T getOneByCondition(String condition, Object[] params) {
		List<T> list = findAllByCondition(condition, params, null);
		if(list == null || list.size() == 0){
			return null;
		}
		
		if(list.size() > 1){
			throw new RuntimeException("getOneByCondition  " + list.size());
		}
		
		return list.get(0);
	}
}
