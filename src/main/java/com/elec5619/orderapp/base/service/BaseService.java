package com.elec5619.orderapp.base.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.elec5619.orderapp.base.dao.BaseDao;

public interface BaseService<T> {
	
	BaseDao<T> getBaseDao();
	
	
	void saveOrUpdate(T entity);
	

	T getEntityById(Serializable id);
	

	void deleteEntityByIds(Serializable... ids);
	

	void deleteEntityByCollection(List<T> list);
	

	List<T> findAllEntity();
	

	List<T> findAllByCondition(String condition,Object[] params, Map<String, String> orderby, Integer page, Integer size);
	

	List<T> findAllByCondition(String condition, Object[] params, Map<String, String> orderby);


	int findTotalRecordsNum(String condition, Object[] params);
	

	T getOneByCondition(String condition,Object[] params);


	List<T> findAllByField(String field,Object value);
	
	
	T findOneByField(String field,Object value);
	
	String countPage();
}