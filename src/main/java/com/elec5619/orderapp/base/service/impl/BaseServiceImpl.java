package com.elec5619.orderapp.base.service.impl;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.elec5619.orderapp.base.dao.BaseDao;
import com.elec5619.orderapp.base.service.BaseService;
import com.elec5619.orderapp.constant.Constant;

@Service
@Transactional
public abstract class BaseServiceImpl<T> implements BaseService<T> {
	
	public abstract BaseDao<T> getBaseDao();
	
	
	public void saveOrUpdate(T entity) {
		getBaseDao().saveOrUpdate(entity);
	}

	
	public T getEntityById(Serializable id) {
		return getBaseDao().getEntityById(id);
	}

	
	public void deleteEntityByIds(Serializable... ids) {
		getBaseDao().deleteEntityByIds(ids);
	}

	public void deleteEntityByCollection(List<T> list) {
		getBaseDao().deleteEntityByCollection(list);
	}

	
	public List<T> findAllEntity() {
		return getBaseDao().findAllEntity();
	}

	
	public List<T> findAllByCondition(String condition, Object[] params, Map<String, String> orderby, Integer page,
			Integer size) {
		return getBaseDao().findAllByCondition(condition, params, orderby,page,size);
	}

	
	public List<T> findAllByCondition(String condition, Object[] params, Map<String, String> orderby) {
		return getBaseDao().findAllByCondition(condition, params, orderby);
	}

	
	public int findTotalRecordsNum(String condition, Object[] params) {
		return getBaseDao().findTotalRecordsNum(condition, params);
	}

	
	public T getOneByCondition(String condition, Object[] params) {
		return getBaseDao().getOneByCondition(condition, params);
	}

	
	public List<T> findAllByField(String field, Object value) {
		return getBaseDao().findAllByField(field, value);
	}

	
	public T findOneByField(String field, Object value) {
		return getBaseDao().findOneByField(field, value);
	}
	
	public String countPage() {
		Integer size = 0;
		int count = getBaseDao().findAllEntity().size();
		if(count % Constant.PAGE_SIZE == 0) {
			size = count / Constant.PAGE_SIZE;
		}else {
			size = count / Constant.PAGE_SIZE + 1;
		}
		
		return size.toString();
	}
}