package com.elec5619.orderapp.constant;

import com.elec5619.orderapp.utils.MD5Utils;

public class Constant {
	
	// user constant
		public final static Integer USER_PWD_WRONG = 0;
		public final static Integer USER_NOT_EXIST = 2;
		public final static Integer USER_EXIST = 3;
		public final static Integer USER_NOT_LOGIN = 0;
		// meal constant
		public final static Integer MS_NOT_EMPTY = 0;
		public final static Integer MS_EMPTY = 3;
		public final static Integer MS_EXIST = 2;
		public final static Integer PAY_REPAET = 2;
		// meal constant
		public final static Integer MEAL_NOT_ORDER = 0;
		public final static Integer MEAL_HAS_ORDER = 3;
		public final static Integer MEAL_EXIST = 2;
		
		public final static Integer SUCCESS = 1;
		
		public final static Integer PAGE_SIZE  = 8;
		public final static Integer USER_PAGE_SIZE  = 9;
		public final static Integer VIEW_PAGE_SIZE  = 4;
		public static String getAdminPassword() {
			return MD5Utils.md5("admin");
		}
		public final static Integer PAGE_OP_ALL = 0;
		public final static Integer PAGE_OP_MS = 1;
		public final static Integer PAGE_OP_KEYWORD = 2;
		public final static String ORDER_STATE = "Payed";
		public final static Integer ORDER_UNCOMMENT = 0;
		public final static Integer ORDER_COMMENT = 1;
		public final static Integer ORDERITEM_COMMENT = 2;

}
