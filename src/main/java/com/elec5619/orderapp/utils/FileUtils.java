package com.elec5619.orderapp.utils;

import java.io.File;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartFile;

public class FileUtils {
	public static String saveFile(HttpServletRequest request,MultipartFile file) {
		try {
			if(file != null && !file.isEmpty()) {
				String path = request.getSession().getServletContext().getRealPath("image");
			    String fileName = file.getOriginalFilename();  
			    String newFileName = UUID.randomUUID() + fileName.substring(fileName.lastIndexOf("."), fileName.length());
			    File targetFile = new File(path, newFileName);  
			    if(!targetFile.exists()){  
			        targetFile.mkdirs();  
			    }  
			    try {  
			        file.transferTo(targetFile);  
			    } catch (Exception e) {  
			        e.printStackTrace();  
			    }  
			    return request.getContextPath()+"/image/"+newFileName;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static void deleteFile(HttpServletRequest request,String path) {
		String filename = path.substring(path.lastIndexOf("/")+1, path.length());
		String realpath = request.getSession().getServletContext().getRealPath("image");
		if(filename != null) {
			File targetFile = new File(realpath, filename);  
			if(targetFile.exists()) {
				targetFile.delete();
			}
		}
		
	}
}
