# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

	The online food ondering system is an application providing services for both customers and sellers.

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
	
	The project is based on SpringMVC framework and Hibernate ORM tools, the developer who wants to install and run the project must set up the relevant components.

* Environment 
	JDK: Above JavaEE 7
	IDE: Eclipse 

* Installation

1. Copy the project git address:

	https://will404@bitbucket.org/will404/elec5619-onlinefood.git

2. Open the IDE

3. Import Git Project and paste the URL

* Configuration 

1. Create the local database

	Using your terminal or workbench to create a database called "orderapp":
	
	create database orderapp;

2. Configure the spring.xml in JavaResources/src/main/resources

3. Set the database properties: "user" and "password" to what you set in your local environment.

4. Using Tomcat 7 as the server (PS: Tomcat 8 cannot be used because of the server choice in the configuration file)

### Testing the project ###

	Using Tomcat 7 as the server and then run the project on the server. If the index page appear on the web browser, it should be successful working.
	The main page's url should be:
	http://localhost:8080/onlineFood/page/index.jsp

	Meanwhile, the developer can typing login.jsp on the URL to login as an admin:
	http://localhost:8080/onlineFood/page/login.jsp